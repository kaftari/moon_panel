<?php

use App\Http\Controllers\Front\DashboardController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\PaymentController;
use App\Http\Controllers\Front\ProductController;
use App\Http\Controllers\Front\SearchController;
use App\Http\Controllers\Panel\AuthController;
use App\Http\Controllers\Panel\CategoryController;
use App\Http\Controllers\Panel\CommentController;
use App\Http\Controllers\Panel\CreatorController;
use App\Http\Controllers\Panel\IndexController;
use App\Http\Controllers\Panel\MigrationOldSiteController;
use App\Http\Controllers\Panel\PostController;
use App\Http\Controllers\Panel\SubscribePlanController;
use App\Http\Controllers\Panel\UsersController;
use Illuminate\Support\Facades\Route;
use UniSharp\LaravelFilemanager\Lfm;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
})->name('home');*/


Route::get('/', [HomeController::class, 'index'])->name('index');

Route::get('/vip-buy', [HomeController::class, 'vip_buy'])->name('vipBuy');

Route::get('/blog', 'BlogController@blog')->name('blog');

Route::get('/about', 'PageController@about')->name('about');

Route::get('/contact', 'PageController@contact')->name('contact');

Route::get('/plus/{slug}', 'PostController@plus_single')->name('plusSingle');

Route::get('/product/{slug}', [ProductController::class, 'single'])->name('productSingle');

Route::get('/product-category/{slug}', 'CategoryController@product_category')->name('productCategory');

Route::get('/posts', 'TestController@show')->name('posts');

Route::get('/search',[SearchController::class,'search'])->name('search');

Route::post('/login', [AuthController::class, 'dashboard_login_perform'])->name('dashboardLoginConfirm');
Route::get('/login', [AuthController::class, 'dashboard_login'])->name('login');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard.', 'middleware' => 'auth'], function () {

    Route::get('/', [DashboardController::class, 'dashboard'])->name('userAccount');
    Route::get('/orders', [DashboardController::class, 'orders'])->name('userOrders');
    Route::get('/downloads', [DashboardController::class, 'downloads'])->name('userDownloads');
    Route::get('/edit-address', [DashboardController::class, 'editAddress'])->name('userEditAddress');
    Route::get('/edit-account', [DashboardController::class, 'editAccount'])->name('userEditAccount');
    Route::post('/post/comment',[ProductController::class,'send_comment'])->name('sendComment');
    Route::get('/logout', [DashboardController::class, 'logout'])->name('logout');
});

Route::group(['prefix'=>'payment','namespace'=>'Payment','as'=>'payment.'],function (){

    Route::get('/', [PaymentController::class, 'payment'])->name('userPayment');
    Route::get('/verify', [PaymentController::class, 'verify'])->name('userPaymentVerify');

});


Route::get('/panel/login', [AuthController::class, 'login'])->name('panelLogin');
Route::post('panel/login/confirm', [AuthController::class, 'login_perform'])->name('loginPerform');
//Route::get('/old/posts',[MigrationOldSiteController::class,'user_migrate'])->name('migrate');

Route::group(['prefix' => 'panel', 'namespace' => 'Panel', 'as' => 'panel.', 'middleware' => ['auth', 'can:admin']], function () {

    Route::get('/index', [IndexController::class, 'index'])->name('index');

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

        #region Posts
        Route::get('/post/create', [PostController::class, 'create'])->name('postCreate');
        Route::post('/post/create', [PostController::class, 'store'])->name('postStore');
        Route::get('/post/json', [PostController::class, 'json'])->name('postJson');
        Route::get('/post/index', [PostController::class, 'index'])->name('postIndex');
        Route::get('/post/edit', [PostController::class, 'edit'])->name('postEdit');
        Route::post('/post/update/{id}', [PostController::class, 'patch'])->name('postPatch');
        Route::get('/post/delete', [PostController::class, 'delete'])->name('postDelete');

        Route::get('/post/replicate', [PostController::class, 'replicate'])->name('postReplicate');
        Route::post('/post/replicate-perform/{id}', [PostController::class, 'replicate_perform'])->name('postReplicatePerform');

        Route::get('/post/image/delete', [PostController::class, 'image_delete'])->name('postImageDelete');
        Route::get('/post/index-image/delete', [PostController::class, 'index_image_delete'])->name('postIndexImageDelete');
        #endregion

        #region Category
        Route::get('/category/json', [CategoryController::class, 'json'])->name('categoryJson');
        Route::get('/category/create', [CategoryController::class, 'create'])->name('categoryCreate');
        Route::post('/category/create', [CategoryController::class, 'store'])->name('categoryStore');
        Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('categoryEdit');
        Route::post('/category/edit/{id}', [CategoryController::class, 'patch'])->name('categoryPatch');
        Route::get('/category/delete', [CategoryController::class, 'json'])->name('categoryDelete');
        #endregion

        #region Creators
        Route::get('/creator/json', [CreatorController::class, 'json'])->name('creatorJson');
        Route::get('/creator/create', [CreatorController::class, 'create'])->name('creatorCreate');
        Route::post('/creator/create', [CreatorController::class, 'store'])->name('creatorStore');
        Route::get('/creator/edit/{id}', [CreatorController::class, 'edit'])->name('creatorEdit');
        Route::post('/creator/edit/{id}', [CreatorController::class, 'patch'])->name('creatorPatch');
        Route::get('/creator/delete', [CreatorController::class, 'json'])->name('creatorDelete');
        #endregion

        #region Users
        Route::get('/users/index', [UsersController::class, 'index'])->name('usersIndex');
        Route::get('/users/json', [UsersController::class, 'json'])->name('usersJson');
        Route::get('/user/create', [UsersController::class, 'create'])->name('userCreate');
        Route::post('/user/create', [UsersController::class, 'store'])->name('userStore');
        Route::get('/user/edit', [UsersController::class, 'edit'])->name('userEdit');
        Route::post('/user/edit', [UsersController::class, 'patch'])->name('userPatch');
        Route::get('/user/delete', [UsersController::class, 'json'])->name('userDelete');
        #endregion

        #region Vip Users
        Route::get('/users/vip/index', [UsersController::class, 'index_user_vip'])->name('usersVipIndex');
        Route::get('/user/renew', [UsersController::class, 'renew'])->name('userRenew');
        Route::post('/user/renew', [UsersController::class, 'patch_renew'])->name('userPatchRenew');
        Route::get('/users/vip/json', [UsersController::class, 'json_vip'])->name('usersVipJson');
        #endregion

        #region Subscribe Plans
        Route::get('/subscribe/plans/json', [SubscribePlanController::class, 'json'])->name('subscribePlanJson');
        Route::get('/subscribe/plans', [SubscribePlanController::class, 'plans'])->name('subscribePlan');
        Route::post('/subscribe/plan/create', [SubscribePlanController::class, 'store'])->name('subscribePlanStore');
        Route::get('/subscribe/plan/edit', [SubscribePlanController::class, 'edit'])->name('subscribePlanEdit');
        Route::post('/subscribe/plan/patch', [SubscribePlanController::class, 'patch'])->name('subscribePlanPatch');
        Route::get('/subscribe/plan/delete', [SubscribePlanController::class, 'delete'])->name('subscribePlanDelete');
        #endregion

        #region Comments
        Route::get('/comments',[CommentController::class,'index'])->name('commentIndex');
        Route::get('/comments/json',[CommentController::class,'json'])->name('commentJson');
        Route::get('/comment/edit',[CommentController::class,'edit'])->name('commentEdit');
        Route::post('/comment/approve',[CommentController::class,'approve'])->name('commentApprove');
        Route::post('/comment/reply',[CommentController::class,'reply'])->name('commentReply');
        Route::get('/comment/delete',[CommentController::class,'delete'])->name('commentDelete');
        #endregion

    });

    Route::get('/test', function () {
        return view('test');
    });

});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    Lfm::routes();
});


