$(function () {

    let categories_table = $('#categories_table');
    $('#categories_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: categories_table.attr('data-href'),
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'slug', name: 'slug'},
            {data: 'post_count', name: 'post_count'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    let posts_table = $('#posts_table');
    $('#posts_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: posts_table.attr('data-href'),
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'title', name: 'title'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    let creators_table = $('#creators_table');
    $('#creators_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: creators_table.attr('data-href'),
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'title', name: 'title'},
            {data: 'slug', name: 'slug'},
            {data: 'post_count', name: 'post_count'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    let users_table = $('#users_table');
    $('#users_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: users_table.attr('data-href'),
        columns: [
            {data: 'username', name: 'username'},
            {data: 'email', name: 'email'},
            {data: 'user_status', name: 'user_status'},
            {data: 'date_register', name: 'date_register'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    let plans_table = $('#plans_table');
    $('#plans_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: plans_table.attr('data-href'),
        columns: [
            {data: 'days', name: 'days'},
            {data: 'price', name: 'price'},
            {data: 'discount', name: 'discount'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    let comments_table = $('#comments_table');
    $('#comments_table').DataTable({
        processing: true,
        serverSide: true,

        ajax: comments_table.attr('data-href'),
        columns: [
            {data: 'user_id', name: 'user_id'},
            {data: 'post_id', name: 'post_id'},
            {data: 'status', name: 'status'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

});





