<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center">Laravel 7 Integrate Ckeditor With Example - XpertPhp</h1><br>
            <form method="post" action="#" class="form form-horizontal">
                @csrf
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" class="form-control"/>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" id="summary-ckeditor" name="summary-ckeditor"></textarea>
                </div>
                <div class="form-group">
                    <label>Author Name</label>
                    <input type="text" name="author" class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('panel/vendors/ckeditor/ckeditor.js') }}"></script>
<script>
    var route_prefix = "/filemanager";
</script>
<script>
    CKEDITOR.replace('summary-ckeditor', {
        height: 500,
        language: 'fa',
        extraPlugins : 'video',
        filebrowserImageBrowseUrl: route_prefix + '?type=Images',
        filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
        filebrowserBrowseUrl: route_prefix + '?type=Files',
        filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'

    });
</script>
</body>
</html>
