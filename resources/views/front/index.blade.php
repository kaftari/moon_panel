﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Main Banner -->
    {{--<section class="main-banner-wrapper">
        <div class="container">
            <div class="banner-wrapper-content">
                <h1>قدم بعدی را به سمت خود بردارید <a href="#" class="typewrite" data-period="2000"
                                                      data-type='[ "طراحی", "وب", "کدنویسی", "جاوا" ]'><span
                            class="wrap"></span></a> توسط راک</h1>
                <p>95٪ افرادی که برای توسعه حرفه ای یاد می گیرند از مزایای شغلی مانند کسب ارتقاء ، افزایش یا شروع کار
                    جدید
                    خبر می دهند.</p>

                <form>
                    <input type="text" class="input-search" placeholder="چه چیز را میخواهی یاد بگیری؟">
                    <button type="button">جستجو</button>
                </form>
            </div>
        </div>

        <div class="banner-wrapper-image text-center">
            <img src="assets/img/banner-img2.png" alt="image">
        </div>
    </section>--}}
    <!-- End Main Banner -->

    <!-- Start Main Banner -->
    <section class="home-slides owl-carousel owl-theme">
        <div class="main-banner item-bg1">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <span class="sub-title">آموزش تحصیلی</span>
                            <h1>به بیرون فکر کنید و یک یادگیرنده را یاد بگیرید</h1>
                            <p>راک با معرفی همکاران خارج از کارآموزی و تجربه تحقیق در خارج از کشور ، از دانشجویان
                                پشتیبانی می کند.</p>

                            <div class="btn-box">
                                <a href="courses-2-columns-style-1.html" class="default-btn"><i
                                        class='bx bx-move-horizontal icon-arrow before'></i><span class="label">مشاهده دوره ها</span><i
                                        class="bx bx-move-horizontal icon-arrow after"></i></a>

                                <a href="contact.html" class="optional-btn">اکنون شروع کن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg2">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <span class="sub-title">آموزش تحصیلی</span>
                            <h1>به بیرون فکر کنید و یک یادگیرنده را یاد بگیرید</h1>
                            <p>راک با معرفی همکاران خارج از کارآموزی و تجربه تحقیق در خارج از کشور ، از دانشجویان
                                پشتیبانی می کند.</p>

                            <div class="btn-box">
                                <a href="courses-2-columns-style-1.html" class="default-btn"><i
                                        class='bx bx-move-horizontal icon-arrow before'></i><span class="label">مشاهده دوره ها</span><i
                                        class="bx bx-move-horizontal icon-arrow after"></i></a>

                                <a href="contact.html" class="optional-btn">اکنون شروع کن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg3">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content text-center">
                            <span class="sub-title">آموزش تحصیلی</span>
                            <h1>به بیرون فکر کنید و یک یادگیرنده را یاد بگیرید</h1>
                            <p>راک با معرفی همکاران خارج از کارآموزی و تجربه تحقیق در خارج از کشور ، از دانشجویان
                                پشتیبانی می کند.</p>

                            <div class="btn-box">
                                <a href="courses-2-columns-style-1.html" class="default-btn"><i
                                        class='bx bx-move-horizontal icon-arrow before'></i><span class="label">مشاهده دوره ها</span><i
                                        class="bx bx-move-horizontal icon-arrow after"></i></a>

                                <a href="contact.html" class="optional-btn">اکنون شروع کن</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Main Banner -->

    <!-- Start Courses Categories Area -->
    <section class="courses-categories-area bg-F7F9FB pt-100 pb-70">
        <div class="container">
            <div class="section-title text-left">
                <span class="sub-title">دسته بندی مطالب</span>
                <h2>دسته بندی های مختلف را مرور کنید</h2>
                {{--<a href="#" class="default-btn"><i
                        class='bx bx-show-alt icon-arrow before'></i><span
                        class="label">مشاهده همه</span><i class="bx bx-show-alt icon-arrow after"></i></a>--}}
            </div>

            <div class="row">
                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="{{route('productCategory',['slug'=>'نرم-افزار'])}}">
                            <i class='bx bx-shape-triangle'></i>
                            نرم افزار
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="{{route('productCategory',['slug'=>'پلاگین'])}}">
                            <i class='bx bx-font-family'></i>
                            پلاگین
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a href="#">
                            <i class='bx bxs-drink'></i>
                            دانش
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="{{route('productCategory',['slug'=>'tutorial'])}}">
                            <i class='bx bx-first-aid'></i>
                            آموزش
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a href="#">
                            <i class='bx bx-bar-chart-alt-2'></i>
                            آبجکت تک
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="#">
                            <i class='bx bx-briefcase-alt-2'></i>
                            مجموعه آبجکت
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="#">
                            <i class='bx bx-book-reader'></i>
                            مجموعه تکستچر
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-courses-category mb-30">
                        <a target="_blank" href="{{route('productCategory',['slug'=>'فروشگاه-مون-آرک'])}}">
                            <i class='bx bx-target-lock'></i>
                            فروشگاه مون آرک
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble-2"></div>
    </section>
    <!-- End Courses Categories Area -->

    <!-- Start Courses Area -->
    <section class="courses-area pt-100 pb-70">
        <div class="container">
            <div class="tab courses-list-tab">
                <ul class="tabs">
                    {{-- <li><a href="#"> آخرین های مون آرک پلاس </a></li>--}}
                    <li><a href="#"> جدید ترین های مون آرک</a></li>

                </ul>

                <div class="tab-content">
                    {{-- <div class="tabs-item">
                         <div class="row">

                             @foreach($plus_objects as $item)
                                 <div class="col-lg-4 col-md-6">

                                     <div class="single-courses-item mb-30">
                                         <div class="courses-image">
                                             <a target="_blank" href="{{route('plusSingle',['slug'=>$item->slug])}}" class="d-block"><img
                                                     src="{{$item->image}}"
                                                     alt="image"></a>
                                         </div>

                                         <div class="courses-content">
                                             <div class="d-flex justify-content-between align-items-center">
                                                 <div class="course-author d-flex align-items-center">
                                                     <img src="assets/img/user1.jpg" class="shadow" alt="image">
                                                     <span>{{array_values($item->terms['galaxy'])[0]}}</span>
                                                 </div>

                                                 --}}{{--<div class="courses-rating">
                                                     <div class="review-stars-rated">
                                                         <i class='bx bxs-star'></i>
                                                         <i class='bx bxs-star'></i>
                                                         <i class='bx bxs-star'></i>
                                                         <i class='bx bxs-star'></i>
                                                         <i class='bx bxs-star-half'></i>
                                                     </div>

                                                     <div class="rating-total">
                                                         4.5 (2)
                                                     </div>
                                                 </div>--}}{{--
                                             </div>

                                             <h3>
                                                 <a target="_blank" href="{{route('plusSingle',['slug'=>$item->slug])}}" class="d-inline-block">
                                                     {{$item->title}}
                                                 </a>
                                             </h3>
                                             --}}{{--                                            <p>آموزش شامل آموزش و یادگیری دانش است.</p>--}}{{--
                                         </div>

                                         --}}{{--<div class="courses-box-footer">
                                             <ul>
                                                 <li class="students-number">
                                                     <i class='bx bx-user'></i> 10 دانشجو
                                                 </li>

                                                 <li class="courses-lesson">
                                                     <i class='bx bx-book-open'></i> 6 درس
                                                 </li>

                                                 <li class="courses-price">
                                                     رایگان
                                                 </li>
                                             </ul>
                                         </div>--}}{{--
                                     </div>
                                 </div>
                             @endforeach
                                 <a href="#" class="default-btn"><i
                                         class='bx bx-show-alt icon-arrow before'></i><span
                                         class="label">مشاهده همه</span><i class="bx bx-show-alt icon-arrow after"></i></a>
                         </div>
                     </div>--}}

                    <div class="tabs-item">
                        <div class="row">
                            @foreach($posts as $item)

                                <div class="col-lg-3 col-md-6 col-sm-6 ">

                                    <div class="single-product-box mb-30 h-100">
                                        <div class="product-image" style="object-fit: cover">
                                            <a href="{{route('productSingle',['slug'=>$item->slug])}}">
                                                <img src="{{$item->getImageThumb()}}" alt="image">
                                                <img src="{{$item->getImageThumb()}}" alt="image">
                                            </a>

                                            <a href="{{route('productSingle',['slug'=>$item->slug])}}"
                                               class="add-to-cart-btn">{{$item->creator['title']}} </a>
                                        </div>

                                        <div class="product-content">
                                            <h6>
                                                {{--<a target="_blank"
                                                   href="{{route('productSingle',['slug'=>$item->slug])}}">
                                                    {{\Illuminate\Support\Str::limit($item->title,20)}}
                                                </a>--}}
                                                <a target="_blank"
                                                   href="{{route('productSingle',['slug'=>$item->slug])}}">
                                                    {{$item->title}}
                                                </a>
                                            </h6>

                                            {{--<div class="price">
                                                <span class="new">45500 تومان</span>
                                            </div>

                                            <div class="rating">
                                                <i class='bx bxs-star'></i>
                                                <i class='bx bxs-star'></i>
                                                <i class='bx bxs-star'></i>
                                                <i class='bx bxs-star'></i>
                                                <i class='bx bxs-star'></i>
                                            </div>--}}

                                        </div>
                                    </div>

                                </div>
                                <br/>
                            @endforeach
                            <br>
                            {{--<a href="#" class="default-btn"><i
                                    class='bx bx-show-alt icon-arrow before'></i><span
                                    class="label">مشاهده همه</span><i class="bx bx-show-alt icon-arrow after"></i></a>--}}
                            {!! $posts->links() !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Courses Area -->

    <!-- Start Features Area -->
    {{--<section class="features-area">
        <div class="container-fluid p-0">
            <div class="row m-0">
                <div class="col-lg-3 col-sm-6 col-md-6 p-0">
                    <div class="single-features-box">
                        <div class="inner-content">
                            <h3>میلیون زبان آموز</h3>
                            <p>دوره های آنلاین رایگان از متخصصان برجسته دنیا. امروز به 13 میلیون زبان آموز بپیوندید.</p>
                            <a href="#" class="default-btn"><i class='bx bx-log-out-circle icon-arrow before'></i><span
                                    class="label">بیشتر بدانید</span><i
                                    class="bx bx-log-out-circle icon-arrow after"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 p-0">
                    <div class="single-features-box">
                        <div class="inner-content">
                            <h3>میلیون فارغ التحصیل</h3>
                            <p>دوره های آنلاین رایگان از متخصصان برجسته دنیا. امروز به 13 میلیون زبان آموز بپیوندید.</p>
                            <a href="#" class="default-btn"><i class='bx bx-log-out-circle icon-arrow before'></i><span
                                    class="label">بیشتر بدانید</span><i
                                    class="bx bx-log-out-circle icon-arrow after"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 p-0">
                    <div class="single-features-box">
                        <div class="inner-content">
                            <h3>دوره های 500K</h3>
                            <p>دوره های آنلاین رایگان از متخصصان برجسته دنیا. امروز به 13 میلیون زبان آموز بپیوندید.</p>
                            <a href="#" class="default-btn"><i class='bx bx-log-out-circle icon-arrow before'></i><span
                                    class="label">بیشتر بدانید</span><i
                                    class="bx bx-log-out-circle icon-arrow after"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-6 p-0">
                    <div class="single-features-box">
                        <div class="inner-content">
                            <h3>145+ کشور دنیا</h3>
                            <p>دوره های آنلاین رایگان از متخصصان برجسته دنیا. امروز به 13 میلیون زبان آموز بپیوندید.</p>
                            <a href="#" class="default-btn"><i class='bx bx-log-out-circle icon-arrow before'></i><span
                                    class="label">بیشتر بدانید</span><i
                                    class="bx bx-log-out-circle icon-arrow after"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="slideshow-box">
            <ul class='slideshow-slides owl-carousel owl-theme'>
                <li><span class="bg3"></span></li>
                <li><span class="bg2"></span></li>
                <li><span class="bg1"></span></li>
                <li><span class="bg4"></span></li>
                <li><span class="bg5"></span></li>
            </ul>
        </div>
    </section>--}}
    <!-- End Features Area -->

    <!-- Start Funfacts Area -->
    {{--<section class="funfacts-style-two ptb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-6">
                    <div class="single-funfact">
                        <div class="icon">
                            <i class='bx bx-group'></i>
                        </div>
                        <h3 style="direction: ltr;" class="odometer" data-count="13">00</h3>
                        <p>میلیون زبان آموز</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-6">
                    <div class="single-funfact">
                        <div class="icon">
                            <i class='bx bxs-graduation'></i>
                        </div>
                        <h3 style="direction: ltr;" class="odometer" data-count="2">00</h3>
                        <p>میلیون فارغ التحصیل</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-6">
                    <div class="single-funfact">
                        <div class="icon">
                            <i class='bx bx-book-reader'></i>
                        </div>
                        <h3 style="direction: ltr;" class="odometer" data-count="2412">00</h3>
                        <p>دوره های مجازی</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-6">
                    <div class="single-funfact">
                        <div class="icon">
                            <i class='bx bx-world'></i>
                        </div>
                        <h3 style="direction: ltr;" class="odometer" data-count="145">00</h3>
                        <p>کشورهای دنیا</p>
                    </div>
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble"></div>
    </section>--}}
    <!-- End Funfacts Area -->

    <!-- Start Team Area -->
    {{--<section class="team-area pt-100 pb-70">
        <div class="container">
            <div class="section-title text-left">
                <span class="sub-title">ارتباطات ما را دنبال کنید</span>
                <h2>تیم مربیان</h2>

                <a href="team-3.html" class="default-btn"><i class='bx bx-show-alt icon-arrow before'></i><span
                        class="label">ملاقات با تیم</span><i class="bx bx-show-alt icon-arrow after"></i></a>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-team-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/1.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">جانکین ژولینور</a></h3>
                            <span>توسعه دهنده</span>
                            <p>جانکین ژولینور دارای سه درجه از دانشگاه هاروارد است.</p>
                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-team-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/2.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">جانکین ژولینور</a></h3>
                            <span>توسعه دهنده</span>
                            <p>جانکین ژولینور دارای سه درجه از دانشگاه هاروارد است.</p>
                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                    <div class="single-team-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/3.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">جانکین ژولینور</a></h3>
                            <span>توسعه دهنده</span>
                            <p>جانکین ژولینور دارای سه درجه از دانشگاه هاروارد است.</p>
                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble-3"></div>
    </section>--}}
    <!-- End Team Area -->

    <!-- Start Feedback Area -->
    {{--<section class="feedback-area ptb-100">
        <div class="container">
            <div class="feedback-slides owl-carousel owl-theme">
                <div class="single-feedback-item">
                    <p>“از افرادی که سعی در کمال جاه طلبی های شما دارند ، دوری کنید. افراد کوچک همیشه این کار را می کنند
                        ،
                        اما واقعاً عالی باعث می شود احساس کنید که شما نیز می توانید عالی باشید.”</p>

                    <div class="info">
                        <h3>جیمز اندرسون</h3>
                        <span>سوئیس</span>
                        <img src="assets/img/user1.jpg" class="shadow rounded-circle" alt="image">
                    </div>
                </div>

                <div class="single-feedback-item">
                    <p>“تفاوت مدرسه و زندگی چیست؟ در مدرسه ، شما یک درس را تدریس می کنید و سپس یک آزمون داده می شوید. در
                        زندگی ، به شما یک آزمون داده می شود که به شما درس می آموزد.”</p>

                    <div class="info">
                        <h3>لیندا سوزان</h3>
                        <span>سوئیس</span>
                        <img src="assets/img/user2.jpg" class="shadow rounded-circle" alt="image">
                    </div>
                </div>

                <div class="single-feedback-item">
                    <p>“اگر مردی کیف خود را درون سر خود خالی کند ، هیچ کس نمی تواند آن را از او دور کند. سرمایه گزاری
                        درعلم
                        همیشه بیشترین سود را در بر دارد.”</p>

                    <div class="info">
                        <h3>دیوید وارمر</h3>
                        <span>سوئیس</span>
                        <img src="assets/img/user3.jpg" class="shadow rounded-circle" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <!-- End Feedback Area -->

    <!-- Start Become Instructor & Partner Area -->
    {{-- <section class="become-instructor-partner-area">
         <div class="container-fluid">
             <div class="row">
                 <div class="col-lg-6 col-md-6">
                     <div class="become-instructor-partner-content bg-color">
                         <h2>تبدیل به یک مربی شوید</h2>
                         <p>از میان صدها دوره رایگان انتخاب کنید یا مدرک را با قیمت دستیابی به موفقیت کسب کنید. با سرعت
                             خود
                             بیاموزید.</p>
                         <a href="login.html" class="default-btn"><i
                                 class='bx bx-plus-circle icon-arrow before'></i><span
                                 class="label">اکنون بپذیر</span><i class="bx bx-plus-circle icon-arrow after"></i></a>
                     </div>
                 </div>

                 <div class="col-lg-6 col-md-6">
                     <div class="become-instructor-partner-image bg-image1 jarallax" data-jarallax='{"speed": 0.3}'>
                         <img src="assets/img/become-instructor.jpg" alt="image">
                     </div>
                 </div>

                 <div class="col-lg-6 col-md-6">
                     <div class="become-instructor-partner-image bg-image2 jarallax" data-jarallax='{"speed": 0.3}'>
                         <img src="assets/img/become-partner.jpg" alt="image">
                     </div>
                 </div>

                 <div class="col-lg-6 col-md-6">
                     <div class="become-instructor-partner-content">
                         <h2>شریک شدن</h2>
                         <p>از میان صدها دوره رایگان انتخاب کنید یا مدرک را با قیمت دستیابی به موفقیت کسب کنید. با سرعت
                             خود
                             بیاموزید.</p>
                         <a href="login.html" class="default-btn"><i
                                 class='bx bx-plus-circle icon-arrow before'></i><span
                                 class="label">تماس با ما</span><i class="bx bx-plus-circle icon-arrow after"></i></a>
                     </div>
                 </div>
             </div>
         </div>
     </section>--}}
    <!-- End Become Instructor & Partner Area -->

@endsection
