﻿@extends('front.layouts.main')
@section('content')

    <section class="pt-1">
        @if (session('success'))
            <div class="alert alert-success text-center">
                <strong>عملیات موفقت آمیز بود !</strong> {{session('success')}}
            </div>
        @elseif(session('error'))
            <div class="alert alert-danger text-center">
                <strong>عملیات با خطا مواجه شد !</strong> {{session('error')}}
            </div>
        @endif
    </section>

    <section class="partner-area pt-70">

        <div class="container">
            <div class="section-title">
                <h2>خرید اعتبار ویژه</h2>
            </div>

            <img src="{{asset('images/banner-vip-but-header-1.jpg')}}" alt="image">

        </div>
    </section>

    <!-- Start Funfacts Area -->
    <section class="funfacts-area pt-70">
        <div class="container">
            <div class="funfacts-inner">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bxs-star'></i>
                            </div>
                            <h5 style="direction: ltr;color: white">کیفیت عالی سرور ها</h5>
                            <p>مون آرک از معدود وبسایت هایی است که چنین حجمی از اطلاعات را درسرور های داخلی کشور نگهداری
                                میکند. این به معنی سرعت و پایداری هرچه بیشتر دانلود های کاربران است.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bx-archive-in'></i>
                            </div>
                            <h5 style="direction: ltr;color: white">آرشیو بی نظیر 3 ترابایتی</h5>
                            <p>آبجکت، تکسچر، آموزش و هرچیز دیگه ای که نیاز داشته باشید. ما در مون آرک با 3 ترابایت
                                اطلاعات غنی و جذاب از شما پذیرایی میکنیم.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bxs-heart'></i>
                            </div>
                            <h5 style="direction: ltr;color: white">پشتیبانی و قیمت خارق العاده</h5>
                            <p>همانطور که شما برای خود و وقتتان ارزش قائلید، ما هم در مون آرک تمام سعی خود را میکنیم تا
                                بهترین خدمات و پشتیبانی را با قیمتی ناچیز در اختیار شما قرار دهیم.</p>
                        </div>
                    </div>
                </div>

                <div id="particles-js-circle-bubble"></div>
            </div>
        </div>
    </section>
    <!-- End Funfacts Area -->

    <!-- Start Courses Categories Area -->
    <section class="courses-categories-area bg-image pt-100 pb-70">
        <div class="container">
            {{-- <div class="section-title">
                 <h2>دسته بندی های گرایش را مرور کنید</h2>
             </div>--}}

            <div class="row">

                @foreach($plans as $key => $plan)
                    <div class="col-lg-3 col-sm-6 col-md-4">
                        <div class="single-categories-courses-box mb-30">
                            <div class="icon">
                                <i class='bx bx-dice-{{$key+1}}'></i>
                            </div>
                            <h2 style="color: white">{{ $plan->days }} روز </h2>
                            <h3>{{$plan->discount. ' تومان ' }} </h3>

                            <a href="{{route('payment.userPayment',['plan'=>$plan->id])}}"
                               class="link-btn"></a>
                        </div>
                    </div>
                @endforeach

                {{--<div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-categories-courses-box mb-30">
                        <div class="icon">
                            <i class='bx bx-dice-2'></i>
                        </div>
                        <h2 style="color: white">45 روز</h2>
                        <h3>54000 تومان</h3>

                        <a href="{{route('payment.userPayment',['days'=>45,'amount'=>54000,'plan'=>2])}}"
                           class="link-btn"></a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4">
                    <div class="single-categories-courses-box mb-30">
                        <div class="icon">
                            <i class='bx bx-dice-3'></i>
                        </div>
                        <h2 style="color: white">60 روز</h2>
                        <h3>72000 تومان</h3>

                        <a href="{{route('payment.userPayment',['days'=>60,'amount'=>72000,'plan'=>3])}}"
                           class="link-btn"></a>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4 offset-lg-0 offset-md-4">
                    <div class="single-categories-courses-box mb-30">
                        <div class="icon">
                            <i class='bx bx-dice-4'></i>
                        </div>
                        <h2 style="color: white">90 روز</h2>
                        <h3>108000 تومان</h3>

                        <a href="{{route('payment.userPayment',['days'=>90,'amount'=>108000,'plan'=>4])}}"
                           class="link-btn"></a>
                    </div>
                </div>--}}

            </div>
        </div>

        <div id="particles-js-circle-bubble-2"></div>
    </section>
    <!-- End Courses Categories Area -->




@endsection

