﻿@extends('layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li><a href="courses-2-columns-style-1.html">{{$post->main_category}}</a></li>
                    <li>{{$post->title}}</li>
                </ul>
                <h2>{{$post->title}}</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Courses Details Area -->
    <section class="courses-details-area pt-100 pb-70">
        <div class="container">
            <div class="courses-details-header">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="courses-title">
                            <h2>{{$post->title}}</h2>
                            <p>{{$post->meta->en_name}}</p>
                        </div>

                        <div class="courses-meta">
                            <ul>
                                <li>
                                    <i class='bx bx-folder-open'></i>
                                    <span>دسته بندی</span>
                                    <a href="#">{{$post->main_category}}</a>
                                </li>
                                <li>
                                    <i class='bx bx-group'></i>
                                    <span>قیمت اصلی</span>
                                    <a href="#">6 دلار</a>
                                </li>
                                <li>
                                    <i class='bx bx-calendar'></i>
                                    <span>تاریخ انتشار</span>
                                    <a href="#">{{\App\Helpers\Date::jalaliToday($post->post_date)}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="courses-price">
                            {{--                            <div class="courses-review">--}}
                            {{--                                <div class="review-stars">--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                </div>--}}
                            {{--                                <span class="reviews-total d-inline-block"></span>--}}
                            {{--                            </div>--}}

                            <div class="price">{{$post->meta->_price.' ت '}}</div>
                            <a href="#" class="default-btn"><i class='bx bx-paper-plane icon-arrow before'></i><span
                                    class="label">پرداخت و دانلود</span><i
                                    class="bx bx-paper-plane icon-arrow after"></i></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <div class="courses-details-image text-center">
                        <img src="{{$post->image}}" alt="image">
                    </div>

                    <!-- Start Gallery Area -->
                    <section class="gallery-area pt-sm-2">
                        <div class="container">
                            <div class="row">
                                @foreach($post->attachment as $image)
                                    <div class="col-lg-3 col-md-9 col-sm-9">
                                        <div class="single-gallery-item mb-30">
                                            <img width="150" height="150" src="{{$image->guid}}" alt="گالری عکس"
                                                 data-original="{{$image->guid}}">
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </section>
                    <!-- End Gallery Area -->

                    <div class="courses-details-desc text-justify">

                        <div class="container" style="line-height: 2;">
                            {!! nl2br($post->post_content) !!}
                        </div>


                        <aside class="widget-area">
                            <section class="widget">
                                <h3 class="widget-title"> درباره مون آرک پلاس</h3>
                            </section>
                        </aside>
                        <h6>مون آرک پلاس با هدف دسترسی راحت تر به منابع ویژه و گران قیمت معماری و سه بعدی آماده سازی شده
                            است. در مون آرک پلاس ابزار های مورد نیاز به صورت تکی و با قیمت بسیار مناسب به فروش می
                            رسند.</h6>
                        <hr/>
                        <ul class="requirements-list">
                            <li>مون آرک پلاس ارتباطی با اعتبار ویژه دانلود بخش های دیگر سایت نداشته و شما بدون داشتن
                                اعتبار ویژه هم قادر به خریداری و استفاده از محصولات مون آرک پلاس هستید.
                                .
                            </li>
                            <li>پس از خرید و پرداخت، محصول به لیست سفارش های شما اضافه خواهد شد و هربار بدون نیاز به
                                خرید دوباره قادر به دریافت محصولات هستید.
                            </li>
                            <li>محصولات در مون آرک پلاس به صورت تکی به فروش می رسند و با خرید هر محصول دسترسی دانلود
                                تنها برای همان محصول تعلق خواهد گرفت.
                            </li>
                        </ul>


                        <aside class="widget-area">
                            <section class="widget">
                                <h3 class="widget-title">ملاقات با متخصصین ما</h3>
                            </section>
                        </aside>
                        <div class="courses-author">
                            <div class="author-profile-header"></div>
                            <div class="author-profile">
                                <div class="author-profile-title">
                                    <img src="{{asset('assets/img/user1.jpg')}}" class="shadow-sm rounded-circle"
                                         alt="image">

                                    <div class="author-profile-title-details d-flex justify-content-between">
                                        <div class="author-profile-details">
                                            <h4>محسن قنادپور</h4>
                                            <span class="d-block">طراح</span>
                                        </div>

                                        <div class="author-profile-raque-profile">
                                            <a href="#" class="d-inline-block">مشاهده پروفایل</a>
                                        </div>
                                    </div>
                                </div>
                                <p>محسن قنادپور یکی از طراحان خوش ذوق گرافیکی است که چندین سال با مون در حال همکاری می
                                    باشد</p>
                                {{--                                <p>لورم ایپسوم به سادگی متن ساختگی صنعت چاپ و تحریر است. لورم ایپسوم صنعت بوده است. لورم--}}
                                {{--                                    ایپسوم به سادگی متن ساختگی صنعت چاپ و تحریر است. لورم ایپسوم صنعت بوده است.</p>--}}
                            </div>
                        </div>


                    </div>

                    <div class="related-courses">
                        <h3>آبجکت های مرتبط</h3>

                        <div class="row">

                            @foreach($related_posts as $post)
                                <div class="col-lg-4 col-md-12">
                                    <div class="single-courses-box mb-30">
                                        <div class="courses-image">
                                            <a target="_blank" href="{{route('plusSingle',['slug'=>$post->slug])}}"
                                               class="d-block"><img
                                                    src="{{$post->image}}" alt="image"></a>

                                            <div class="courses-tag">
                                                <a href="#" class="d-block">{{$post->main_category}}</a>
                                            </div>
                                        </div>

                                        <div class="courses-content">
                                            <div class="course-author d-flex align-items-center">
                                                <img src="{{asset('assets/img/user1.jpg')}}" class="rounded-circle mr-2"
                                                     alt="image">
                                                <span>{{$post->author->display_name}}</span>
                                            </div>

                                            <h3>
                                                <a target="_blank" href="{{route('plusSingle',['slug'=>$post->slug])}}"
                                                   class="d-inline-block">
                                                    {{$post->title}}
                                                </a>
                                            </h3>

                                            {{--                                            <div class="courses-rating">--}}
                                            {{--                                                <div class="review-stars-rated">--}}
                                            {{--                                                    <i class='bx bxs-star'></i>--}}
                                            {{--                                                    <i class='bx bxs-star'></i>--}}
                                            {{--                                                    <i class='bx bxs-star'></i>--}}
                                            {{--                                                    <i class='bx bxs-star'></i>--}}
                                            {{--                                                    <i class='bx bxs-star'></i>--}}
                                            {{--                                                </div>--}}

                                            {{--                                                <div class="rating-total">--}}
                                            {{--                                                    5.0 (1 امتیاز)--}}
                                            {{--                                                </div>--}}
                                            {{--                                            </div>--}}

                                        </div>

                                        {{--                                        <div class="courses-box-footer">--}}
                                        {{--                                            <ul>--}}
                                        {{--                                                <li class="students-number">--}}
                                        {{--                                                    <i class='bx bx-user'></i> 10 دانشجو--}}
                                        {{--                                                </li>--}}

                                        {{--                                                <li class="courses-lesson">--}}
                                        {{--                                                    <i class='bx bx-book-open'></i> 6 درس--}}
                                        {{--                                                </li>--}}

                                        {{--                                                <li class="courses-price">--}}
                                        {{--                                                    رایگان--}}
                                        {{--                                                </li>--}}
                                        {{--                                            </ul>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-12">



                            <h3 class="widget-title">محبوبترین آبجکت ها</h3>

                            @foreach($related_posts as $post)
                                <article class="item">
                                    <a href="{{route('plusSingle',['slug'=>$post->slug])}}">
                                        <img src="{{$post->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_THUMBNAIL)['url']}}"/>
                                        <span class="fullimage cover bg1" role="img"></span>
                                    </a>
                                    <div class="info">
                                        <time datetime="2019-06-30">{{$post->meta->_price}} تومان</time>
                                        <h4 class="title usmall"><a
                                                href="{{route('plusSingle',['slug'=>$post->slug])}}">{{$post->title}}</a>
                                        </h4>
                                    </div>

                                    <div class="clear"></div>
                                </article>
                            @endforeach



                </div>
            </div>
        </div>
    </section>
    <!-- End Courses Details Area -->
@endsection
