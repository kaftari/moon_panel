﻿<!doctype html>
<html lang="fa">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="HTML5,CSS3,HTML,Template,multi-page,Raque - Education & LMS HTML Template">
    <meta name="description" content="Raque - Education & LMS HTML Template">
    <meta name="author" content="Barat Hadian">

    <!-- Links of CSS files -->
    <link rel="stylesheet" href="assets/css/bootstrap..min.css">
    <link rel="stylesheet" href="assets/css/boxicons.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/odometer.min.css">
    <link rel="stylesheet" href="assets/css/meanmenu.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.min.css">
    <link rel="stylesheet" href="assets/css/viewer.min.css">
    <link rel="stylesheet" href="assets/css/slick.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/rtl.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <title>{{config('app.name')}}</title>

    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>

<body>

<!-- Preloader -->
<div class="preloader">
    <div class="loader">
        <div class="shadow"></div>
        <div class="box"></div>
    </div>
</div>
<!-- End Preloader -->



<!-- Start Login Area -->
<section style="background-color: #00aaff" class="login-area">
    <div class="row m-0">
        <div class="col-lg-6 col-md-12 p-0">
            <div class="login-image">
                <img src="assets/img/login-bg.jpg" alt="image">
            </div>
        </div>


        <div class="col-lg-6 col-md-12 p-0">
            <div class="login-content">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="login-form">
                            @if (session('error'))

                                <div class="alert alert-danger">
                                    <strong>Danger!</strong> {{session('error')}}
                                </div>
                            @endif
                            <div class="logo">
{{--                                <a href="index.html"><img src="assets/img/black-logo.png" alt="image"></a>--}}
                                <img width="250px" src="{{asset('assets/img/LogoNew.png')}}" alt="logo">
                            </div>

                            <h3>خوش آمدید</h3>
                            <p>میخای کاربر جدید مون آرک بشی؟ <a href="#">ثبت نام کن</a></p>

                            <form method="post" action="{{route('dashboardLoginConfirm')}}">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="username" placeholder="نام کاربری" class="form-control">
                                </div>

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror


                                <div class="form-group">
                                    <input type="password" name="password" placeholder="رمز عبور" class="form-control">
                                </div>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror

                                <button type="submit">ورود</button>

                                <div class="forgot-password">
                                    <a href="#">رمز عبور را فراموش کرده اید؟</a>
                                </div>

                                {{--<div class="connect-with-social">
                                    <button type="submit" class="facebook"><i class='bx bxl-facebook'></i> اتصال با فیسبوک</button>
                                    <button type="submit" class="twitter"><i class='bx bxl-twitter'></i> اتصال با توئیتر</button>
                                </div>--}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Login Area -->

<!-- Links of JS files -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/mixitup.min.js"></script>
<script src="assets/js/parallax.min.js"></script>
<script src="assets/js/jquery.appear.min.js"></script>
<script src="assets/js/odometer.min.js"></script>
<script src="assets/js/particles.min.js"></script>
<script src="assets/js/meanmenu.min.js"></script>
<script src="assets/js/jquery.nice-select.min.js"></script>
<script src="assets/js/viewer.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/jquery.ajaxchimp.min.js"></script>
<script src="assets/js/form-validator.min.js"></script>
<script src="assets/js/contact-form-script.js"></script>
<script src="assets/js/main.js"></script>
</body>

</html>
