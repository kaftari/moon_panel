<!doctype html>
<html lang="fa">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="HTML5,CSS3,HTML,Template,multi-page,Raque - Education & LMS HTML Template" >
    <meta name="description" content="Raque - Education & LMS HTML Template">
    <meta name="author" content="Barat Hadian">

    <!-- Links of CSS files -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/boxicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/odometer.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/meanmenu.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/viewer.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/rtl.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

    <title>{{config('app.name')}}</title>

    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">

    <style>
        .dlbox {
            background: #00aaff;
            border-radius: 5px;
            color: #fff;
            padding: 15px;
            margin-top: 15px;
            position: relative;
        }
        .dlbox .headline {
            border-bottom: 1px solid #fff;
            font-size: 16px;
            padding-bottom: 10px;
            margin-bottom: 10px;
            color: #08171f;
        }
        .dlboxmsg {
            position: absolute;
            top: 350px;
            right: 15px;
            left: 15px;
            background: #fff;
            color: #555;
            border-radius: 5px;
            -webkit-box-shadow: 0px 0px 10px rgba(0,0,0,0.5);
            box-shadow: 0px 0px 10px rgba(0,0,0,0.5);
            padding: 10px 0;
            margin-top: 10px;
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
            -o-transform: scale(0);
            transform: scale(0);
            opacity: 0;
            z-index: 9999;
        }
        .dlbox .links {
            margin-top: 15px;
        }
        .passandhelp {
            border-top: 2px dotted rgba(0,0,0,0.2);
            padding-top: 15px;
            margin-top: 15px;
        }
        .passandhelp button {
            padding: 0 15px;
            line-height: 2;
            height: auto;
            margin: 0;
            border-color: rgba(255,255,255,0.8);
            color: rgba(255,255,255,0.8);
            border-width: 1px;
            font-weight: bold;
        }
        .passwordbox {
            border: 1px solid rgba(255,255,255,0.8);
            color: rgba(255,255,255,0.8);
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            -o-border-radius: 5px;
            border-radius: 5px;
            padding: 0 15px;

            line-height: 2;
        }
        .passwordbox div.passwordinput {
            float: left;
            width: auto;
            height: auto;
            padding: 0;
            line-height: 1;
            font: bold 15px/2.4 IRANSans;
            direction: ltr;
            text-align: left;
            background: transparent;
            border: none;
            display: inline;
            margin: 0;
        }
        input[type="submit"], button {
            height: 35px;
            border: 2px solid #00aaff;
            color: #00aaff;
            padding: 0 10px;
            /*font: 14px/30px IRANSans;*/
            font: 14px/30px;
            background: transparent;
            border-radius: 5px;
            margin: 5px 0;
            cursor: pointer;
            display: inline-block;
            -webkit-transition: all 0.4s;
            -moz-transition: all 0.4s;
            -ms-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
        }
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        user agent stylesheet
        button {
            appearance: button;
            -webkit-writing-mode: horizontal-tb !important;
            text-rendering: auto;
            color: -internal-light-dark(black, white);
            letter-spacing: normal;
            word-spacing: normal;
            text-transform: none;
            text-indent: 0px;
            text-shadow: none;
            display: inline-block;
            text-align: center;
            align-items: flex-start;
            cursor: default;
            background-color: -internal-light-dark(rgb(239, 239, 239), rgb(59, 59, 59));
            box-sizing: border-box;
            margin: 0em;
            font: 400 13.3333px Arial;
            padding: 1px 6px;
            border-width: 2px;
            border-style: outset;
            border-color: -internal-light-dark(rgb(118, 118, 118), rgb(133, 133, 133));
            border-image: initial;
        }
        .dlbox .links .a {
            display: block;
            width: 100%;
            margin-bottom: 10px;
            padding-bottom: 10px;
            border-bottom: 1px dotted rgba(255,255,255,0.5);
            line-height: 20px;
        }
        .passandhelp button:hover {
            background: #fff;
            color: #00aaff;
        }
        .full-width {
            width: 100%;
        }
        .dlbox .headline i {
            margin-left: 5px;
        }
        .dlbox .headline .shine {
            margin-right: 10px;
            -webkit-animation: shineondark 4s infinite;
            -o-animation: shineondark 4s infinite;
            animation: shineondark 3s infinite;
            color: white;
        }
        .dlbox .headline span.badgak {
            float: left;
            background: #fff;
            display: inline-block;
            padding: 0px 10px;
            color: #08171f;
            margin-left: -15px;
            margin-top: -15px;
            border-radius: 0 0 15px 0;
        }
        .dlboxmsg div a {
            float: left;
            padding: 3px 15px;
            background: #2ab737;
            border-radius: 5px;
            border: 0px solid #1e8127;
            border-width: 0px 0px 3px 2px;
            margin-top: 0;
            margin-right: 10px;
        }
        .dlboxmsg div a.grey {
            float: left;
            padding: 3px 15px;
            background: #aaa;
            border-radius: 5px;
            border: 0px solid #888;
            border-width: 0px 0px 3px 2px;
            margin-top: 0;
            margin-right: 10px;
        }
        .dlbox .links .plain {


            /*font-family: IRANSans;*/
            text-align: center;
            background: rgba(0,0,0,0.07);
            border-bottom: 1px dotted rgba(255,255,255,0.7);
            margin-bottom: 8px;
            margin-top: -8px;
            padding: 8px 10px 8px;
        }


        .dlbox .links .a {
            display: block;
            width: 100%;
            margin-bottom: 10px;
            padding-bottom: 10px;
            border-bottom: 1px dotted rgba(255,255,255,0.5);
            line-height: 20px;
            color: #fff;
            font-weight: bold;
        }
        .dlbox .links a:hover {
            color: red;
        }
        .dlbox .links .a span {
            float: left;
            color: #fff;
            font: bold 12px/2 Arial;
        }
    </style>

</head>

<body>

<!-- Preloader -->
<div class="preloader">
    <div class="loader">
        <div class="shadow"></div>
        <div class="box"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Header Area -->
<header class="header-area p-relative">

    <!-- Start Navbar Area -->
    <div class="navbar-area navbar-style-four">
        <div class="raque-responsive-nav">
            <div class="container">
                <div class="raque-responsive-menu">
                    <div class="logo">
                        <a href="{{route('index')}}">
                            <img width="250px" src="{{asset('assets/img/LogoNew.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="raque-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="{{route('index')}}">
                        <img  width="250px"  src="{{asset('assets/img/LogoNew.png')}}" alt="logo">
                    </a>

                    <div class="collapse navbar-collapse mean-menu">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="{{route('index')}}" class="nav-link active">صفحه اصلی </a>
                            </li>

                            <li class="nav-item"><a href="{{route('dashboard.userAccount')}}" class="nav-link">حساب کاربری</a>
                            </li>

                            <li class="nav-item"><a href="{{route('vipBuy')}}" class="nav-link">اشتراک ویژه</a>
                            </li>

                            <li class="nav-item"><a target="_blank" href="{{route('blog')}}" class="nav-link">وبلاگ</a>
                            </li>

                            <li class="nav-item"><a href="#" class="nav-link">صفحات راهنما <i class='bx bx-chevron-down'></i></a>
                                <ul class="dropdown-menu">

                                    <li class="nav-item"><a href="feedback.html" class="nav-link">راهنمای خرید</a></li>

                                    <li class="nav-item"><a href="partner.html" class="nav-link">راهنمای دانلود</a></li>

                                    <li class="nav-item"><a href="partner.html" class="nav-link">خرید کلی</a></li>

                                    <li class="nav-item"><a href="login.html" class="nav-link">قوانین</a></li>

                                </ul>
                            </li>

                            <li class="nav-item"><a target="_blank" href="{{route('contact')}}" class="nav-link">تماس با ما</a></li>

                            <li class="nav-item"><a target="_blank" href="{{route('about')}}" class="nav-link">درباره مون آرک</a></li>

                        </ul>

                        <div class="others-option">


                            <a href="#" class="cart-wrapper-btn d-inline-block">
                                <i class='bx bx-cart-alt'></i>
                                <span>0</span>
                            </a>

                            <div class="search-box d-inline-block">
                                <i class='bx bx-search'></i>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Navbar Area -->

    <!-- Start Sticky Navbar Area -->
    <div class="navbar-area navbar-style-four header-sticky">
        <div class="raque-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="{{route('index')}}">
                        <img width="250px" src="{{asset('assets/img/LogoNew.png')}}" alt="logo">
                    </a>

                    <div class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="{{route('index')}}" class="nav-link active">صفحه اصلی </a>
                            </li>

                            <li class="nav-item"><a href="#" class="nav-link">حساب کاربری</a>
                            </li>

                            <li class="nav-item"><a href="{{route('vipBuy')}}" class="nav-link">اشتراک ویژه</a>
                            </li>

                            <li class="nav-item"><a target="_blank" href="{{route('blog')}}" class="nav-link">وبلاگ</a>
                            </li>

                            <li class="nav-item"><a href="#" class="nav-link">صفحات راهنما <i class='bx bx-chevron-down'></i></a>
                                <ul class="dropdown-menu">

                                    <li class="nav-item"><a href="feedback.html" class="nav-link">راهنمای خرید</a></li>

                                    <li class="nav-item"><a href="partner.html" class="nav-link">راهنمای دانلود</a></li>

                                    <li class="nav-item"><a href="partner.html" class="nav-link">خرید کلی</a></li>

                                    <li class="nav-item"><a href="login.html" class="nav-link">قوانین</a></li>

                                </ul>
                            </li>

                            <li class="nav-item"><a target="_blank" href="{{route('contact')}}" class="nav-link">تماس با ما</a></li>

                            <li class="nav-item"><a target="_blank" href="{{route('about')}}" class="nav-link">درباره مون آرک</a></li>

                        </ul>

                        <div class="others-option">

                            {{--                            <div class="dropdown language-switcher d-inline-block">--}}
                            {{--                                <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                            {{--                                    <img src="{{asset('assets/img/iran-flag.jpg')}}" class="shadow" alt="image">--}}
                            {{--                                    <span>فا <i class='bx bx-chevron-down'></i></span>--}}
                            {{--                                </button>--}}

                            {{--                                <div class="dropdown-menu">--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/us-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Eng</span>--}}
                            {{--                                    </a>--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/germany-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Ger</span>--}}
                            {{--                                    </a>--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/france-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Fre</span>--}}
                            {{--                                    </a>--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/spain-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Spa</span>--}}
                            {{--                                    </a>--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/russia-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Rus</span>--}}
                            {{--                                    </a>--}}
                            {{--                                    <a href="#" class="dropdown-item d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('assets/img/italy-flag.jpg')}}" class="shadow-sm" alt="flag">--}}
                            {{--                                        <span>Ita</span>--}}
                            {{--                                    </a>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}

                            <a href="#" class="cart-wrapper-btn d-inline-block">
                                <i class='bx bx-cart-alt'></i>
                                <span>0</span>
                            </a>

                            <div class="search-box d-inline-block">
                                <i class='bx bx-search'></i>
                            </div>
                        </div>
                    </div>

                </nav>
            </div>
        </div>
    </div>
    <!-- End Sticky Navbar Area -->

</header>
<!-- End Header Area -->

<!-- search-box-layout -->
<div class="search-overlay">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="search-overlay-layer"></div>
            <div class="search-overlay-layer"></div>
            <div class="search-overlay-layer"></div>

            <div class="search-overlay-close">
                <span class="search-overlay-close-line"></span>
                <span class="search-overlay-close-line"></span>
            </div>

            <div class="search-overlay-form">
                <form method="get" action="{{route('search')}}">
                    <input type="text" class="input-search" name="search" placeholder="جستجو ...">
                    <button type="submit"><i class='bx bx-search-alt'></i></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- search-box-layout end -->


@yield('content')

<!-- Start Footer Area -->
<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget mb-30">
                    <h3>تماس بگیرید</h3>

                    <ul class="contact-us-link">
                        <li>
                            <i class='bx bx-map'></i>
                            <a href="#" target="_blank">کشور شما ، استان و شهر شما ، محله و منطقه شما</a>
                        </li>
                        <li>
                            <i class='bx bx-phone-call'></i>
                            <a href="#">021-12345678</a>
                        </li>
                        <li>
                            <i class='bx bx-envelope'></i>
                            <a href="#">hello@raque.com</a>
                        </li>
                    </ul>

                    <ul class="social-link">
                        <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-facebook'></i></a></li>
                        <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                        <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-instagram'></i></a></li>
                        <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-linkedin'></i></a></li>
                        <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-pinterest-alt'></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="single-footer-widget mb-30">
                    <h3>پشتیبانی</h3>

                    <ul class="support-link">
                        <li><a href="#">حریم خصوصی</a></li>
                        <li><a href="#">سوالات متداول</a></li>
                        <li><a href="#">پشتیبانی</a></li>
                        <li><a href="#">قوانین</a></li>
                        <li><a href="#">شرایط و ضوابط</a></li>
                        <li><a href="#">مقررات</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="single-footer-widget mb-30">
                    <h3>لینکهای مفید</h3>

                    <ul class="useful-link">
                        <li><a href="#">طراحی وب</a></li>
                        <li><a href="#">طراحی رابط کاربری</a></li>
                        <li><a href="#">توسعه دهنده</a></li>
                        <li><a href="#">برنامه وب</a></li>
                        <li><a href="#">خبرنامه</a></li>
                        <li><a href="#">توسعه یاب</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="single-footer-widget mb-30">
                    <h3>خبرنامه</h3>

                    <div class="newsletter-box">
                        <p>برای دریافت آخرین اخبار و آخرین به روزرسانی های ما</p>

                        <form class="newsletter-form" data-toggle="validator">
                            <label>ایمیل شما:</label>
                            <input type="email" class="input-newsletter" placeholder="ایمیل خود را وارد کنید" name="EMAIL" required autocomplete="off">
                            <button type="submit">مشترک شدن</button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-area">
        <div class="container">
            <div class="logo">
{{--                <a href="index-4.html" class="d-inline-block"><img src="{{asset('assets/img/logo.png')}}" alt="image"></a>--}}
            </div>
            <p>کپی رایت 1399 <i class='bx bx-copyright'></i> تمام حقوق قالب محفوظ است. طراحی و توسعه توسط تیم توسعه مون آرک</p>
        </div>
    </div>
</footer>
<!-- End Footer Area -->

<div class="go-top"><i class='bx bx-up-arrow-alt'></i></div>

<!-- Links of JS files -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/js/mixitup.min.js')}}"></script>
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.appear.min.js')}}"></script>
<script src="{{asset('assets/js/odometer.min.js')}}"></script>
<script src="{{asset('assets/js/particles.min.js')}}"></script>
<script src="{{asset('assets/js/meanmenu.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('assets/js/viewer.min.js')}}"></script>
<script src="{{asset('assets/js/slick.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('assets/js/form-validator.min.js')}}"></script>
<script src="{{asset('assets/js/contact-form-script.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>

