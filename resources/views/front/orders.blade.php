﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg2 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>حساب کاربری من</li>
                    <li>سفارشات</li>
                </ul>
                <h2>حساب کاربری من</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start My Account Area -->
    <section class="my-account-area ptb-100">
        <div class="container">

            @include('front.partials.account.navbar')

            <div class="myAccount-content">
                <div class="orders-table table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>سفارش</th>
                            <th>تاریخ</th>
                            <th>وضعیت</th>
                            <th>مجموع</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>#074145O5</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O6</td>
                            <td>18 دی 1398</td>
                            <td>در انتظار</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O7</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O5</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O6</td>
                            <td>18 دی 1398</td>
                            <td>در انتظار</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O7</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->

@endsection
