﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li><a href="courses-2-columns-style-1.html">
                            {{--                            {{$post->main_category}}--}}
                        </a></li>
                    <li>{{$post->title}}</li>
                </ul>
                <h2>{{$post->title}}</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Product Details Area -->
    <section class="product-details-area pt-100 pb-70">
        <div class="container">
            {{--            <div class="row align-items-center">--}}
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <div class="products-details-image">

                        <img width="100%" height="400px" src="{{$post->image}}" alt="image"></li>

                        <section class="gallery-area pt-50">
                            <div class="container">
                                <div class="row">
                                    @foreach($post->images as $image)
                                        <div class="col-lg-4 col-md-6 col-sm-6">
                                            <div class="single-gallery-item mb-30">
                                                <img src="{{$image->image}}" alt="گالری عکس"
                                                     data-original="{{$image->image}}">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>

                        {{--<ul class="slickslide">
                            <li><img width="100%" height="400px" src="{{$post->image}}" alt="image"></li>
                            @foreach($post->images as $image)
                                <li><img width="100%" height="400px" src="{{$image->image}}" alt="image"></li>
                            @endforeach
                        </ul>

                        <div class="slick-thumbs">
                            <ul>
                                <li><img width="100px" height="100px" src="{{$post->image}}" alt="image"></li>
                                @foreach($post->images as $image)
                                    <li><img width="100px" height="100px" src="{{$image->image}}" alt="image"></li>
                                @endforeach
                            </ul>
                        </div>--}}

                    </div>
                </div>

                <div class="col-lg-7 col-md-12">
                    <div class="product-details-desc">

                        <h3>{{$post->title}}</h3>
                        {{--                        <h6>{{$post->meta->en_name}}</h6>--}}

                        {{--<div class="price">
                            <span class="new-price">54100 تومان</span>
                            <span class="old-price">65200 تومان</span>
                        </div>--}}

                        <div class="product-review">
                            <div class="rating">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star-half'></i>
                            </div>

                            {{--                            <a href="#" class="rating-count">3 بازدید</a>--}}
                        </div>

                        {{--<p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                            صنعت بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40
                            سال استاندارد صنعت بوده است.</p>--}}

                        <div class="product-meta">
                            {{--<span>سازنده: <span class="sku">{{$product_creator}}</span></span>
                            <span>شاخه:
                                <span class="in-stock">
                                    @foreach($product_categories as $category)
                                        {{ $category.'  ' }}
                                    @endforeach
                                </span>
                            </span>
                            <span>حجم پس از اکسترکت: <a href="#">10.4 گیگابایت</a></span>
                            <span>تاریخ انتشار: <a
                                    href="#">{{\App\Helpers\Date::jalaliToday($post->post_date)}}</a></span>
                            <span>بازدید: <a href="#">71 بار</a></span>--}}


                            <aside class="widget-area">

                                <section class="widget widget_events_details">
                                    <h3 class="widget-title">اطلاعات کلی</h3>

                                    <ul>
                                        <li>
                                            <span> تعداد بازدید:   </span> {{$post_views}}
                                        </li>
                                        <li>
                                            <span> سازنده:   </span> {{$post->creator ? $post->creator['title'] : 'نامشخص'}}
                                        </li>
                                        <li><span> حجم فایل بعد از اکسترکت:   </span> {{$post->size}}
                                            گیگابایت
                                        </li>
                                        <li>
                                            <span> تاریخ انتشار: </span> {{\App\Helpers\Date::jalaliToday($post->created_at)}}
                                        </li>
                                        <li><span> دسته بندی: </span>
                                            @php $last_key = (array_keys($categories)); @endphp
                                            @foreach($post->categories as $key => $category)
                                                <a href="#">
                                                    {{$category->name}}
                                                    {{(end($last_key) == $key) ? ' ' : ' , '}}
                                                </a>
                                            @endforeach
                                            {{--@foreach($post->categories as $category)
                                                <a href="#">{{$category->name}}</a>
                                            @endforeach--}}
                                        </li>
                                    </ul>
                                </section>

                                <section class="widget widget_tag_cloud">
                                    <h3 class="widget-title">برچسب ها</h3>

                                    <div class="tagcloud">
                                        @foreach($tags as $tag)
                                            <a href="#">{{$tag .' '}}<span class="tag-link-count"></span></a>
                                        @endforeach
                                    </div>
                                </section>
                            </aside>

                        </div>

                        {{--                        <div class="product-add-to-cart">
                                                    <div class="input-counter">
                                                        <span class="minus-btn"><i class='bx bx-minus'></i></span>
                                                        <input type="text" min="1" value="1">
                                                        <span class="plus-btn"><i class='bx bx-plus'></i></span>
                                                    </div>

                                                    <button type="submit" class="default-btn"><i class='bx bx-plus icon-arrow before'></i><span
                                                            class="label">افزودن سبد خرید</span><i class="bx bx-plus icon-arrow after"></i>
                                                    </button>
                                                </div>

                                                <div class="custom-payment-options">
                                                    <span>پرداخت مطمئن تضمین شده:</span>

                                                    <div class="payment-methods">
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/1.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/2.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/3.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/4.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/5.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/6.svg" alt="image"></a>
                                                        <a href="#"><img src="{{asset('')}}assets/img/payment/7.svg" alt="image"></a>
                                                    </div>
                                                </div>--}}
                    </div>
                </div>

                @if($post->video_url)
                    <div class="col-lg-12 col-md-12">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item"
                                    src="https://www.aparat.com/video/video/embed/videohash/{{$post->video_url}}/vt/frame"></iframe>
                        </div>
                    </div>
                @endif

                <div class="col-lg-12 col-md-12">
                    <div class="tab products-details-tab">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <ul class="tabs">
                                    <li><a href="#">
                                            <div class="dot"></div>
                                            شرح
                                        </a></li>

                                    {{--<li><a href="#">
                                            <div class="dot"></div>
                                            لینک های دانلود
                                        </a>
                                    </li>--}}

                                    <li><a href="#">
                                            <div class="dot"></div>
                                             نظرات  {{ '('.count($post->comments).')' }}
                                        </a></li>
                                </ul>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <div class="tab-content">

                                    @include('front.partials.single.download_links')

                                    {{--<div class="tabs-item">
                                        <div class="products-details-tab-content">

                                        </div>
                                    </div>--}}

                                    @include('front.partials.single.comments')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="related-products">
            <div class="container">
                <div class="section-title text-left">
                    {{--                    <span class="sub-title">فروشگاه ما</span>--}}
                    <span class="sub-title"></span>
                    <h2>محصولات مشابه</h2>
                </div>

                {{--Related Post--}}
                <div class="row">

                    @foreach($related_posts as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="single-product-box mb-30">
                                    <div class="product-image">
                                        <a href="{{route('productSingle',['slug'=>$item->slug])}}">
                                            <img src="{{$item->image}}" alt="image">
                                            <img src="{{$item->image}}" alt="image">
                                        </a>

{{--                                                                            <a href="#" class="add-to-cart-btn">افزودن سبد خرید <i class='bx bx-cart'></i></a>--}}
                                    </div>

                                    <div class="product-content">
                                        <h6><a target="_blank" href="{{route('productSingle',['slug'=>$item->slug])}}">{{$item->title}}</a></h6>

                                        {{--<div class="price">
                                            <span class="new">45500 تومان</span>
                                        </div>

                                        <div class="rating">
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                            <i class='bx bxs-star'></i>
                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                    @endforeach

                </div>

            </div>
        </div>
    </section>
    <!-- End Product Details Area -->

@endsection
{{--@section('css')

@stop--}}


