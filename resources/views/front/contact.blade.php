﻿@extends('layouts.main')
@section('content')
    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg2 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li>تماس با ما</li>
                </ul>
                <h2>تماس با ما</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Contact Info Area -->
    <section class="contact-info-area pt-100 pb-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="contact-info-box mb-30">
                        <div class="icon">
                            <i class='bx bx-envelope'></i>
                        </div>

                        <h3>ایمیل ما</h3>
                        <p><a href="mailto:hello@raque.com">Info@moonarch.ir</a></p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="contact-info-box mb-30">
                        <div class="icon">
                            <i class='bx bx-map'></i>
                        </div>

                        <h3>آدرس ما</h3>
                        <p><a href="#" target="_blank">ایران - کرج</a></p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                    <div class="contact-info-box mb-30">
                        <div class="icon">
                            <i class='bx bx-phone-call'></i>
                        </div>

                        <h3>تماس بگیرید</h3>
                        <p><a href="tel:09120347003">09120347003</a></p>
                    </div>
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble-2"></div>
    </section>
    <!-- End Contact Info Area -->

    <!-- Start Contact Area -->
    <section class="contact-area pb-100">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">تماس با ما</span>
                <h2>با ما در ارتباط باشید</h2>
                <p>شما میتوانید از طریق فرم زیر و یا اطلاعات موجود در این صفحه با ما در تماس باشید. شنیدن هر خبر، پیشنهاد، انتقاد و یا نیاز به راهنمایی از شما باعث خوشحالی ماست</p>
            </div>

            <div class="contact-form">
                <form id="contactForm">
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <input type="text" name="name" id="name" class="form-control" required
                                       data-error="لطفا نام خود را وارد کنید" placeholder="نام شما">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" required
                                       data-error="ایمیل خود را وارد کنید" placeholder="ایمیل شما">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <input type="text" name="phone_number" id="phone_number" required
                                       data-error="تلفن خود را وارد کنید" class="form-control" placeholder="تلفن">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-12">
                            <div class="form-group">
                                <input type="text" name="msg_subject" id="msg_subject" class="form-control" required
                                       data-error="موضوع پیام خود را بنویسید" placeholder="موضوع شما">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <textarea name="message" class="form-control" id="message" cols="30" rows="5" required
                                          data-error="پیام خود را وارد کنید" placeholder="پیام شما"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="default-btn"><i
                                    class='bx bx-paper-plane icon-arrow before'></i><span
                                    class="label">ارسال پیام</span><i class="bx bx-paper-plane icon-arrow after"></i>
                            </button>
                            <div id="msgSubmit" class="h3 text-center hidden"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="particles-js-circle-bubble-3"></div>
        <div class="contact-bg-image"><img src="assets/img/map.png" alt="image"></div>
    </section>
    <!-- End Contact Area -->

@endsection
