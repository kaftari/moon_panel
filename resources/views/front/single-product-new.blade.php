﻿@extends('layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li><a href="courses-2-columns-style-1.html">{{$post->main_category}}</a></li>
                    <li>{{$post->title}}</li>
                </ul>
                <h2>{{$post->title}}</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Product Details Area -->
    <section class="product-details-area pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="products-details-image">
                        <ul class="slickslide">
                            @foreach($post->attachment as $image)
                                <li><img  width="100%" height="400px" src="{{$image->guid}}" alt="image"></li>
                            @endforeach
                        </ul>

                        <div class="slick-thumbs">
                            <ul>
                                @foreach($post->attachment as $image)
                                    <li><img width="100px" height="100px" src="{{$image->guid}}" alt="image"></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-md-12">
                    <div class="product-details-desc">
                        <h3>{{$post->title}}</h3>
                        <h6>{{$post->meta->en_name}}</h6>


                        {{--<div class="price">
                            <span class="new-price">{{$post->meta->_price}}  تومان </span>
                            --}}{{--<span class="old-price">65200 تومان</span>--}}{{--
                        </div>--}}

                        <div class="product-review">
                            <div class="rating">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star-half'></i>
                            </div>

                            <a href="#" class="rating-count">3 بازدید</a>
                        </div>

{{--                        <p class="text-justify"> {!! nl2br($post->post_content) !!}</p>--}}

                        <div class="product-meta">
                            <span>قیمت : <span class="sku">{{$post->meta->_price}}  تومان </span></span>
                            <span>قیمت اصلی : <span class="in-stock">6 دلار</span></span>
                            <span>دسته بندی : <a href="#">{{$post->main_category}}</a></span>
                            <span>تاریخ انتشار : <a href="#">{{\App\Helpers\Date::jalaliToday($post->post_date)}}</a></span>
                            <span>برچسب : <a href="#">کتاب</a></span>
                        </div>

                        <div class="product-add-to-cart">
                            <div class="input-counter">
                                <span class="minus-btn"><i class='bx bx-minus'></i></span>
                                <input type="text" min="1" value="1">
                                <span class="plus-btn"><i class='bx bx-plus'></i></span>
                            </div>

                            <button type="submit" class="default-btn"><i class='bx bx-plus icon-arrow before'></i><span
                                    class="label">افزودن سبد خرید</span><i class="bx bx-plus icon-arrow after"></i>
                            </button>
                        </div>

                        <div class="custom-payment-options">
                            <span>پرداخت مطمئن تضمین شده:</span>

                            <div class="payment-methods">
                                <a href="#"><img src="{{asset('')}}assets/img/payment/1.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/2.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/3.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/4.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/5.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/6.svg" alt="image"></a>
                                <a href="#"><img src="{{asset('')}}assets/img/payment/7.svg" alt="image"></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="tab products-details-tab">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <ul class="tabs">
                                    <li><a href="#">
                                            <div class="dot"></div>
                                            توضیحات
                                        </a></li>

                                    <li><a href="#">
                                            <div class="dot"></div>
                                            لینک های دانلود
                                        </a></li>

                                    <li><a href="#">
                                            <div class="dot"></div>
                                            نظرات
                                        </a></li>
                                </ul>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <div class="tab-content">
                                    <div class="tabs-item">
                                        <div class="products-details-tab-content">
                                            <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به
                                                مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به سادگی ساختار چاپ و
                                                متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است.
                                                لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به
                                                مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به سادگی ساختار چاپ و
                                                متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده است.
                                                لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به
                                                مدت 40 سال استاندارد صنعت بوده است.</p>
                                        </div>
                                    </div>

                                    <div class="tabs-item">
                                        <div class="products-details-tab-content">
                                            <ul class="additional-information">
                                                <li><span>برند:</span> تم فارست</li>
                                                <li><span>رنگبندی:</span> قهوه ای</li>
                                                <li><span>اندازه:</span> بزرگ ، متوسط</li>
                                                <li><span>وزن:</span> 27 کیلو</li>
                                                <li><span>ابعاد:</span> 16 x 22 x 123 سانتی متر</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tabs-item">
                                        <div class="products-details-tab-content">
                                            <div class="product-review-form">
                                                <h3>نظرات مشتری</h3>

                                                <div class="review-title">
                                                    <div class="rating">
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bxs-star'></i>
                                                        <i class='bx bx-star'></i>
                                                    </div>
                                                    <p>بر اساس 3 بررسی</p>

                                                    <a href="#" class="default-btn"><i
                                                            class='bx bx-plus icon-arrow before'></i><span
                                                            class="label">یک نظر بنویسید</span><i
                                                            class="bx bx-plus icon-arrow after"></i></a>
                                                </div>

                                                <div class="review-comments">
                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bx-star'></i>
                                                        </div>
                                                        <h3>عالی</h3>
                                                        <span><strong>مدیر</strong> در <strong>21 دی 1398</strong></span>
                                                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم
                                                            ایپسوم به مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به
                                                            سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت
                                                            40 سال استاندارد صنعت بوده است.</p>
                                                    </div>

                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bx-star'></i>
                                                        </div>
                                                        <h3>عالی</h3>
                                                        <span><strong>مدیر</strong> در <strong>21 دی 1398</strong></span>
                                                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم
                                                            ایپسوم به مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به
                                                            سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت
                                                            40 سال استاندارد صنعت بوده است.</p>
                                                    </div>

                                                    <div class="review-item">
                                                        <div class="rating">
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bxs-star'></i>
                                                            <i class='bx bx-star'></i>
                                                        </div>
                                                        <h3>عالی</h3>
                                                        <span><strong>مدیر</strong> در <strong>21 دی 1398</strong></span>
                                                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم
                                                            ایپسوم به مدت 40 سال استاندارد صنعت بوده است. لورم ایپسوم به
                                                            سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت
                                                            40 سال استاندارد صنعت بوده است.</p>
                                                    </div>
                                                </div>

                                                <div class="review-form">
                                                    <h3>نظر بدهید</h3>

                                                    <form>
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="text" id="name" name="name"
                                                                           placeholder="نام شما" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-6 col-md-6">
                                                                <div class="form-group">
                                                                    <input type="email" id="email" name="email"
                                                                           placeholder="ایمیل شما" class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <input type="text" id="review-title"
                                                                           name="review-title" placeholder="موضوع نظر"
                                                                           class="form-control">
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="form-group">
                                                                    <textarea name="review-body" id="review-body"
                                                                              cols="30" rows="6"
                                                                              placeholder="پیام خود را بنویسید"
                                                                              class="form-control"></textarea>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <button type="submit" class="default-btn"><i
                                                                        class='bx bx-paper-plane icon-arrow before'></i><span
                                                                        class="label">ارسال نظر</span><i
                                                                        class="bx bx-paper-plane icon-arrow after"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="related-products">
            <div class="container">
                <div class="section-title text-left">
                    <span class="sub-title">فروشگاه ما</span>
                    <h2>محصولات مشابه</h2>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-product-box mb-30">
                            <div class="product-image">
                                <a href="single-product.html">
                                    <img src="{{asset('')}}assets/img/shop/1.jpg" alt="image">
                                    <img src="{{asset('')}}assets/img/shop/1-1.jpg" alt="image">
                                </a>

                                <a href="#" class="add-to-cart-btn">افزودن سبد خرید <i class='bx bx-cart'></i></a>
                            </div>

                            <div class="product-content">
                                <h3><a href="single-product.html">دسته رمان</a></h3>

                                <div class="price">
                                    <span class="new">45500 تومان</span>
                                </div>

                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-product-box mb-30">
                            <div class="product-image">
                                <a href="single-product.html">
                                    <img src="{{asset('')}}assets/img/shop/2.jpg" alt="image">
                                    <img src="{{asset('')}}assets/img/shop/2-1.jpg" alt="image">
                                </a>

                                <a href="#" class="add-to-cart-btn">افزودن سبد خرید <i class='bx bx-cart'></i></a>

                                <div class="sale-btn">ویژه!</div>
                            </div>

                            <div class="product-content">
                                <h3><a href="single-product.html">کتاب رمان</a></h3>

                                <div class="price">
                                    <span class="old">65200 تومان</span>
                                    <span class="new">54100 تومان</span>
                                </div>

                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star-half'></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-product-box mb-30">
                            <div class="product-image">
                                <a href="single-product.html">
                                    <img src="{{asset('')}}assets/img/shop/3.jpg" alt="image">
                                    <img src="{{asset('')}}assets/img/shop/3-1.jpg" alt="image">
                                </a>

                                <a href="#" class="add-to-cart-btn">افزودن سبد خرید <i class='bx bx-cart'></i></a>
                            </div>

                            <div class="product-content">
                                <h3><a href="single-product.html">کتاب قصه</a></h3>

                                <div class="price">
                                    <span class="new">14000 تومان</span>
                                </div>

                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bx-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Product Details Area -->

@endsection


