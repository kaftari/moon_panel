﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>حساب کاربری من</li>
                </ul>
                <h2>آدرس ها</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start My Account Area -->
    <section class="my-account-area ptb-100">
        <div class="container">

            @include('front.partials.account.navbar')

            <div class="myAccount-content">
                <p>آدرس های زیر به طور پیش فرض در صفحه پرداخت استفاده می شود.</p>

                <div class="myAccount-addresses">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="addresses-title">
                                <h3>آدرس صورتحساب</h3>
                                <a href="edit-billing-address.html" class="edit">ویرایش</a>
                            </div>
                            <address>
                                جیمز اندرسون
                                <br>
                                ایران
                                <br>
                                خیابان شرقی 4
                                <br>
                                استان تهران بزرگ
                            </address>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="addresses-title">
                                <h3>آدرس حمل و نقل</h3>
                                <a href="edit-shipping-address.html" class="edit">افزودن</a>
                            </div>
                            <address>شما هنوز این نوع آدرس را تنظیم نکردید.</address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->

@endsection
