﻿@extends('front.layouts.main')
@section('content')
    <!-- Start Team Area -->
    <section class="team-area pt-100 pb-70">
        <div class="container">
            <div class="row">

                @if(!count($posts))
                    <h1>نتیجه ای برای جستجوی شما پیدا نشد!</h1>
                @endif
                @foreach($posts as $post)
                    <div class="col-lg-3 col-md-6 col-sm-6 ">
                        <div class="single-instructor-box mb-30  h-100">
                            <div class="image" style="object-fit: cover">
                                <img src="{{$post->getImageThumb()}}" alt="image">

                                {{-- <ul class="social">
                                     <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                     <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                     <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                     <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                                 </ul>--}}
                            </div>

                            <div class="content text-left">
                                <h6><a href="{{route('productSingle',['slug' => $post->slug])}}">{{$post->title}}</a>
                                </h6>
                                <span>{{$post->creator['title']}}</span>
                            </div>
                        </div>
                    </div>
                @endforeach

                {{--  <div class="col-lg-4 col-md-6 col-sm-6">
                      <div class="single-instructor-box mb-30">
                          <div class="image">
                              <img src="assets/img/team/2.jpg" alt="image">

                              <ul class="social">
                                  <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                              </ul>
                          </div>

                          <div class="content">
                              <h3><a href="single-instructor.html">ژانکین جولینور</a></h3>
                              <span>سئوکار وب</span>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-6 col-sm-6">
                      <div class="single-instructor-box mb-30">
                          <div class="image">
                              <img src="assets/img/team/3.jpg" alt="image">

                              <ul class="social">
                                  <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                              </ul>
                          </div>

                          <div class="content">
                              <h3><a href="single-instructor.html">ژانکین جولینور</a></h3>
                              <span>سئوکار وب</span>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-6 col-sm-6">
                      <div class="single-instructor-box mb-30">
                          <div class="image">
                              <img src="assets/img/team/4.jpg" alt="image">

                              <ul class="social">
                                  <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                              </ul>
                          </div>

                          <div class="content">
                              <h3><a href="single-instructor.html">ژانکین جولینور</a></h3>
                              <span>سئوکار وب</span>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-6 col-sm-6">
                      <div class="single-instructor-box mb-30">
                          <div class="image">
                              <img src="assets/img/team/5.jpg" alt="image">

                              <ul class="social">
                                  <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                              </ul>
                          </div>

                          <div class="content">
                              <h3><a href="single-instructor.html">ژانکین جولینور</a></h3>
                              <span>سئوکار وب</span>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-4 col-md-6 col-sm-6">
                      <div class="single-instructor-box mb-30">
                          <div class="image">
                              <img src="assets/img/team/6.jpg" alt="image">

                              <ul class="social">
                                  <li><a href="#" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                                  <li><a href="#" target="_blank"><i class="bx bxl-instagram"></i></a></li>
                              </ul>
                          </div>

                          <div class="content">
                              <h3><a href="single-instructor.html">ژانکین جولینور</a></h3>
                              <span>سئوکار وب</span>
                          </div>
                      </div>
                  </div>--}}

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="pagination-area text-center">
                    {{$posts->links()}}
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble-3"></div>
    </section>
    <!-- End Team Area -->
@endsection

