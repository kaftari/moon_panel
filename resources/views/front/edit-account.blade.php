﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg4 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>حساب کاربری من</li>
                    <li>ویرایش</li>
                </ul>
                <h2>حساب کاربری من</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start My Account Area -->
    <section class="my-account-area ptb-100">
        <div class="container">

            @include('front.partials.account.navbar')

            <div class="myAccount-content">
                <form class="edit-account">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>نام شما <span class="required">*</span></label>
                                <input type="text" class="form-control" value="جیمز">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>نام خانوادگی <span class="required">*</span></label>
                                <input type="text" class="form-control" value="اندرسون">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>نام مستعار <span class="required">*</span></label>
                                <input type="text" class="form-control" value="جیمز ادس">
                                <span><em>اینگونه خواهد بود که نام شما در بخش حساب و در نظرات نمایش داده می شود</em></span>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>آدرس ایمیل <span class="required">*</span></label>
                                <input type="email" class="form-control" value="hello@jamesanderson.com">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <legend>تغییر رمز عبور</legend>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>گذرواژه فعلی (خالی بگذارید بدون تغییر)</label>
                                <input type="password" class="form-control">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="form-group">
                                <label>گذرواژه جدید (خالی بگذارید بدون تغییر)</label>
                                <input type="password" class="form-control">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label>رمزعبور جدید را تأیید کنید</label>
                                <input type="password" class="form-control">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="default-btn"><i class='bx bx-save icon-arrow before'></i><span
                                    class="label">ذخیره تغییرات</span><i class="bx bx-save icon-arrow after"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->

@endsection
