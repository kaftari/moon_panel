﻿@extends('layouts.main')
@section('content')
    <!-- Start Page Title Area -->
    <div class="page-title-area page-title-style-three item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li>وبلاگ</li>
                </ul>
                <h2></h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Blog Area -->
    <section class="blog-area ptb-100">
        <div class="container-fluid">
            <div class="row">

                @foreach($posts as $post)
                    <div class="col-lg-3 col-md-6">

                        <div class="single-blog-post mb-30">
                            <div class="post-image">
                                <a href="#" class="d-block">
                                    <img src="{{$post->image}}" alt="image">
                                </a>

                                <div class="tag">
                                    <a href="#">{{$post->main_category}}</a>
                                </div>
                            </div>

                            <div class="post-content">
                                <ul class="post-meta">
                                    <li class="post-author">
                                        <img src="assets/img/user1.jpg" class="d-inline-block rounded-circle mr-2"
                                             alt="image">
                                        توسط: <a href="#" class="d-inline-block">{{$post->author->display_name}}</a>
                                    </li>
                                    <li><a href="#">{{$post->post_date}}  </a></li>
                                </ul>
                                <h3>
                                    <a href="#" class="d-inline-block">
                                        {{$post->title}}
                                    </a>
                                </h3>
                                <a href="#" class="read-more-btn">ادامه خواندن <i class='bx bx-left-arrow-alt'></i></a>
                            </div>
                        </div>
                    </div>

                @endforeach

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pagination-area text-center">
                        {{$posts->links()}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Blog Area -->
@endsection
