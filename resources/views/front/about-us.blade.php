﻿@extends('layouts.main')
@section('content')
    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>درباره ما</li>
                </ul>
                <h2>درباره ما</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start About Area -->
    <section class="about-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="about-image">
                        <img src="assets/img/about/1.jpg" class="shadow" alt="image">
                        <img src="assets/img/about/2.jpg" class="shadow" alt="image">
                    </div>
                </div>

                <div class="col-lg-6 col-md-12">
                    <div class="about-content">
                        <span class="sub-title">درباره ما</span>

                        <p>در مون آرک تلاش ما بر این است که آخرین ابزار نرم افزاری هر معمار و طراحی را با روش هایی برای دسترسی ساده تر مثل خرید محصولات به صورت فیزیکی درب منزل، دانلود رایگان و ویژه محصولات کم یاب برای شما همراهان مون آرک آماده کنیم. شما می توانید محصولات و ابزار مورد نیاز خودرا مستقیما از سرور های پر سرعت ما دانلود کنید، بر روی هارددیسک و دیسک درب منزل دریافت کنید و با خرید هر یک از آیتم های مون آرک پلاس میتوانید با قیمتی بسیار پایین تر از تمام منابع اصلی با کیفیت ترین و کمیاب ترین آبجت های روز دنیا را مستقیما دریافت کنید.</p>

                        <p>یکی از بزرگترین اهداف مون آرک همیشه فراهم آوردن شرایطی بوده که در آن شاهد کمترین محدودیت های اعمال شده به واسطه تحریم ها برای کاربران ایرانی باشیم.</p>

                        <p>ما در مون آرک سعی میکنیم با فراهم آوردن بایگانی وسیعی از ابزارآلات و محصولات مرتبط با معماری، طراحی سه بعدی و گرافیک به کاربران سراسر کشور کمک کنیم با کمترین هزینه به موارد مد نظر خود در یک محل به صورت دسته بندی شده و تخصصی دسترسی پیدا کنند. در مدت بسیار کوتاه سپری شده از زمان راه اندازی مون آرک شاهد این بودیم که هر روز کاربران بیشتری با ما همراه می شوند و توانسته ایم با رضایت جلب کاربران محترم خود به یکی از قوی ترین سایت های مرتبط در تمام سطح کشور تبدیل شویم.</p>

                        <div class="features-text">
                            <h5><i class='bx bx-planet'></i>مکانی که می توانید به آن برسید</h5>

                        </div>
                    </div>
                </div>
            </div>

            <div class="about-inner-area">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="about-text">
                            <h3>100000 دوره آنلاین</h3>
                            <p>نوآوری های واقعی و یک تجربه مثبت مشتری قلب ارتباطات موفق است.</p>

                            <ul class="features-list">
                                <li><i class='bx bx-check'></i> گوش دادن را فعال کنید</li>
                                <li><i class='bx bx-check'></i> ذهن های درخشان</li>
                                <li><i class='bx bx-check'></i> بهترین ها را انتخاب کنید!</li>
                                <li><i class='bx bx-check'></i> بهترین برندینگ!</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="about-text">
                            <h3>دستورالعمل ویژه</h3>
                            <p>نوآوری های واقعی و یک تجربه مثبت مشتری قلب ارتباطات موفق است.</p>

                            <ul class="features-list">
                                <li><i class='bx bx-check'></i> ایجاد نتایج.</li>
                                <li><i class='bx bx-check'></i> انتظار بیشتر</li>
                                <li><i class='bx bx-check'></i> خوب فکر کردن</li>
                                <li><i class='bx bx-check'></i> در واقع ما اعتماد داریم</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                        <div class="about-text">
                            <h3>دسترسی به طول عمر</h3>
                            <p>نوآوری های واقعی و یک تجربه مثبت مشتری قلب ارتباطات موفق است.</p>

                            <ul class="features-list">
                                <li><i class='bx bx-check'></i> واقعی بمان همیشه.</li>
                                <li><i class='bx bx-check'></i> ما شما را آموزش می دهیم</li>
                                <li><i class='bx bx-check'></i> خوب توجه کنید</li>
                                <li><i class='bx bx-check'></i> افزایش اعتماد برندینگ</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Area -->

    <!-- Start Mission Area -->
    <section class="mission-area ptb-100 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="mission-content">
                <div class="section-title text-left">
                    <span class="sub-title">ماموریت را کشف کنید</span>
                    <h2>چرا بسترهای پلتفرم ما</h2>
                </div>

                <div class="mission-slides owl-carousel owl-theme">
                    <div>
                        <h3>کیفیت می تواند در این سیستم عامل از کمیت بهتر باشد</h3>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                            صنعت
                            بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال
                            استاندارد صنعت بوده است.</p>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                            صنعت
                            بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال
                            استاندارد صنعت بوده است.</p>
                        <a href="about-style-2.html" class="default-btn"><i
                                class='bx bx-user-pin icon-arrow before'></i><span class="label">بیشتر بخوانید</span><i
                                class="bx bx-user-pin icon-arrow after"></i></a>
                    </div>

                    <div>
                        <h3>مکانی که می توانید به آن برسید</h3>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                            صنعت
                            بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال
                            استاندارد صنعت بوده است.</p>
                        <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                            صنعت
                            بوده است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال
                            استاندارد صنعت بوده است.</p>
                        <a href="about-style-1.html" class="default-btn"><i
                                class='bx bx-user-pin icon-arrow before'></i><span class="label">بیشتر بخوانید</span><i
                                class="bx bx-user-pin icon-arrow after"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Mission Area -->

    <!-- Start Story Area -->
    <section class="story-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="section-title text-left">
                        <h2>داستان ما</h2>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12">
                    <div class="story-content">
                        <h3>آینده بهتر از اینجا شروع می شود</h3>
                        <p>پس از سالها فعالیت در صنعت میزبانی وب ، متوجه شدیم که به طور متوسط بعضی ها نمی توانند وب سایت
                            خود
                            را ایجاد کنند. خدمات میزبانی وب سنتی به سادگی مدیریتی بسیار پیچیده ، وقت گیر و گران قیمت
                            بودند.</p>
                        <h3>آموزش کلاسیک برای آینده</h3>
                        <p>ما سازنده سایت وب سایت را با در نظر داشتن دیدگاه کاربر ایجاد کردیم. ما می خواستیم بستری را
                            ارائه
                            دهیم که نیازی به مهارت برنامه نویسی و تجربه طراحی نداشته باشد. ما آن را ساده نگه می داریم ،
                            بنابراین کاربران می توانند در ایجاد یک وب سایت شگفت انگیز که مارک آنها را تعیین می کند
                            متمرکز
                            شوند. بهترین از همه - این برنامه رایگان است. می توانید آنلاین شوید ، مارک خود را به نمایش
                            بگذارید ، یا فوراً شروع به فروش محصولات کنید.</p>
                        <h3>سفری به جهان</h3>
                        <p>پس از مشاهده نیاز بیشتر به راه حل های تجارت الکترونیک ، ما یکی از تنها سازندگان فروشگاه
                            آنلاین
                            کاملاً برجسته ، رایگان و بدون کمیسیون را توسعه دادیم و به صاحبان مشاغل اجازه داد تا تجارت
                            آنلاین
                            خود را راه اندازی کنند..</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Story Area -->

    <!-- Start Funfacts Area -->
    <section class="funfacts-area">
        <div class="container">
            <div class="funfacts-inner">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bxs-group'></i>
                            </div>
                            <h3 style="direction: ltr;" class="odometer" data-count="50">00</h3>
                            <p>مربیان خبره</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bx-book-reader'></i>
                            </div>
                            <h3 style="direction: ltr;" class="odometer" data-count="1754">00</h3>
                            <p>تمام دوره ها</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bx-user-pin'></i>
                            </div>
                            <h3 style="direction: ltr;" class="odometer" data-count="8190">00</h3>
                            <p>دانش آموزان راضی</p>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-6">
                        <div class="single-funfact">
                            <div class="icon">
                                <i class='bx bxl-deviantart'></i>
                            </div>
                            <h3 style="direction: ltr;" class="odometer" data-count="654">00</h3>
                            <p>رویداد خلاقانه</p>
                        </div>
                    </div>
                </div>

                <div id="particles-js-circle-bubble"></div>
            </div>
        </div>
    </section>
    <!-- End Funfacts Area -->

    <!-- Start Our Values Area -->
    <section class="values-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-12">
                    <div class="section-title text-left">
                        <h2>ارزش های ما</h2>
                    </div>
                </div>

                <div class="col-lg-8 col-md-12">
                    <div class="values-content">
                        <h3>یک محیط یادگیری موفقیت محور</h3>
                        <p>پس از سالها فعالیت در صنعت میزبانی وب ، متوجه شدیم که به طور متوسط بعضی ها نمی توانند وب سایت
                            خود
                            را ایجاد کنند. خدمات میزبانی وب سنتی به سادگی مدیریتی بسیار پیچیده ، وقت گیر و گران قیمت
                            بودند.</p>
                        <h3>آموزش دانشگاهی و تنوع فرهنگی</h3>
                        <p>ما سازنده سایت وب سایت را با در نظر داشتن دیدگاه کاربر ایجاد کردیم. ما می خواستیم بستری را
                            ارائه
                            دهیم که نیازی به مهارت برنامه نویسی و تجربه طراحی نداشته باشد. ما آن را ساده نگه می داریم ،
                            بنابراین کاربران می توانند در ایجاد یک وب سایت شگفت انگیز که نشان دهنده نام تجاری آنها است ،
                            تمرکز کنند. از همه بهتر اینکه مجانیه. می توانید آنلاین شوید ، مارک خود را به نمایش بگذارید ،
                            یا
                            فوراً شروع به فروش محصولات کنید.</p>
                        <h3>پیشبرد درک انسانی</h3>
                        <p>پس از مشاهده نیاز بیشتر به راه حل های تجارت الکترونیک ، ما یکی از تنها سازندگان فروشگاه
                            آنلاین
                            کاملاً برجسته ، رایگان و بدون کمیسیون را توسعه دادیم و به صاحبان مشاغل اجازه داد تا تجارت
                            آنلاین
                            خود را راه اندازی کنند..</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Our Values Area -->

    <!-- Start Team Area -->
    <section class="team-area pb-100">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">ارتباطات را برقرار کنید</span>
                <h2>تیم مربیان</h2>
                <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت بوده
                    است.</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-instructor-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/1.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">الکس ماکسول</a></h3>
                            <span>سئوکار وب</span>

                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-instructor-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/2.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">الکس ماکسول</a></h3>
                            <span>سئوکار وب</span>

                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                    <div class="single-instructor-member mb-30">
                        <div class="member-image">
                            <img src="assets/img/team/3.jpg" alt="images">
                        </div>

                        <div class="member-content">
                            <h3><a href="single-instructor.html">الکس ماکسول</a></h3>
                            <span>سئوکار وب</span>

                            <ul class="social">
                                <li><a href="#" class="facebook" target="_blank"><i class='bx bxl-facebook'></i></a>
                                </li>
                                <li><a href="#" class="twitter" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="instagram" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="linkedin" target="_blank"><i class='bx bxl-linkedin'></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="team-btn-box text-center">
                        <a href="team-1.html" class="default-btn"><i class='bx bx-show-alt icon-arrow before'></i><span
                                class="label">ملاقات با مربیان</span><i class="bx bx-show-alt icon-arrow after"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div id="particles-js-circle-bubble-3"></div>
    </section>
    <!-- End Team Area -->

    <!-- Start Partner Area -->
    <section class="partner-area bg-color ptb-100">
        <div class="container">
            <div class="section-title">
                <h2>
                    شرکت و همکاران ما
                </h2>
            </div>

            <div class="partner-slides owl-carousel owl-theme">
                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/1.png" alt="image">
                    </a>
                </div>

                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/2.png" alt="image">
                    </a>
                </div>

                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/3.png" alt="image">
                    </a>
                </div>

                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/4.png" alt="image">
                    </a>
                </div>

                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/5.png" alt="image">
                    </a>
                </div>

                <div class="single-partner-item">
                    <a href="#" class="d-block">
                        <img src="assets/img/partner/6.png" alt="image">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Partner Area -->

    <!-- Start Testimonials Area -->
    <section class="testimonials-area ptb-100">
        <div class="container">
            <div class="section-title">
                <span class="sub-title">مشتریان</span>
                <h2>آنچه دانشجویان می گویند</h2>
            </div>

            <div class="testimonials-slides owl-carousel owl-theme">
                <div class="single-testimonials-item">
                    <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت
                        بوده
                        است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                        صنعت بوده است.</p>

                    <div class="info">
                        <img src="assets/img/user1.jpg" class="shadow rounded-circle" alt="image">
                        <h3>جان اسمیت</h3>
                        <span>دانشجو</span>
                    </div>
                </div>

                <div class="single-testimonials-item">
                    <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت
                        بوده
                        است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                        صنعت بوده است.</p>

                    <div class="info">
                        <img src="assets/img/user2.jpg" class="shadow rounded-circle" alt="image">
                        <h3>جان اسمیت</h3>
                        <span>دانشجو</span>
                    </div>
                </div>

                <div class="single-testimonials-item">
                    <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت
                        بوده
                        است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                        صنعت بوده است.</p>

                    <div class="info">
                        <img src="assets/img/user3.jpg" class="shadow rounded-circle" alt="image">
                        <h3>جان اسمیت</h3>
                        <span>دانشجو</span>
                    </div>
                </div>

                <div class="single-testimonials-item">
                    <p>لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد صنعت
                        بوده
                        است. لورم ایپسوم به سادگی ساختار چاپ و متن را در بر می گیرد. لورم ایپسوم به مدت 40 سال استاندارد
                        صنعت بوده است.</p>

                    <div class="info">
                        <img src="assets/img/user4.jpg" class="shadow rounded-circle" alt="image">
                        <h3>جان اسمیت</h3>
                        <span>دانشجو</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonials Area -->

    <!-- Start Become Instructor & Partner Area -->
    <section class="become-instructor-partner-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="become-instructor-partner-content bg-color">
                        <h2>تبدیل به یک مربی شوید</h2>
                        <p>از میان صدها دوره رایگان انتخاب کنید یا مدرک را با قیمت دستیابی به موفقیت کسب کنید. با سرعت
                            خود
                            بیاموزید.</p>
                        <a href="login.html" class="default-btn"><i
                                class='bx bx-plus-circle icon-arrow before'></i><span
                                class="label">اکنون بپذیر</span><i class="bx bx-plus-circle icon-arrow after"></i></a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="become-instructor-partner-image bg-image1 jarallax" data-jarallax='{"speed": 0.3}'>
                        <img src="assets/img/become-instructor.jpg" alt="image">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="become-instructor-partner-image bg-image2 jarallax" data-jarallax='{"speed": 0.3}'>
                        <img src="assets/img/become-partner.jpg" alt="image">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="become-instructor-partner-content">
                        <h2>شریک شدن</h2>
                        <p>از میان صدها دوره رایگان انتخاب کنید یا مدرک را با قیمت دستیابی به موفقیت کسب کنید. با سرعت
                            خود
                            بیاموزید.</p>
                        <a href="login.html" class="default-btn"><i
                                class='bx bx-plus-circle icon-arrow before'></i><span
                                class="label">تماس با ما</span><i class="bx bx-plus-circle icon-arrow after"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Become Instructor & Partner Area -->
@endsection

