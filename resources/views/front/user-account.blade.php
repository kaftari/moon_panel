﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li>حساب کاربری من</li>
                </ul>
                <h2>حساب کاربری من</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start My Account Area -->
    <section class="my-account-area ptb-100">
        <div class="container">
            <div class="myAccount-profile">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-5">
                        <div class="profile-image">
                            <img src="assets/img/team/1.jpg" alt="image">
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-7">
                        <div class="profile-content">
                            <h3>{{\Illuminate\Support\Facades\Auth::user()->name}}</h3>
                            <p>خوش آمدید</p>

                            <ul class="contact-info">
                                <li><i class='bx bx-envelope'></i>
                                    <a href="#">{{\Illuminate\Support\Facades\Auth::user()->email}}</a>
                                </li>
                                <li><i class='bx bx-user-check'></i>

                                    @if(\App\Helpers\Users::checkUserStatus())
                                        {{'روزهای باقی مانده از اشتراک شما :'}}
                                        {{\App\Helpers\Date::finding_days_between_two_dates() . ' روز '}}
                                    @else
                                        <p>شما اشتراک ویژه ندارید</p>
                                    @endif

                                </li>
                                <li><i class='bx bx-world'></i> <a href="#" target="_blank"></a>
                                </li>
                            </ul>
                            <ul class="social">
                                <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-facebook'></i></a></li>
                                <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-twitter'></i></a></li>
                                <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-instagram'></i></a>
                                </li>
                                <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-linkedin'></i></a></li>
                                <li><a href="#" class="d-block" target="_blank"><i class='bx bxl-pinterest-alt'></i></a>
                                </li>
                            </ul>

                            <a href="{{route('dashboard.logout')}}" class="myAccount-logout">خروج</a>
                        </div>
                    </div>
                </div>
            </div>

            @include('front.partials.account.navbar')

            <div class="myAccount-content">
                <p>سلام <strong>{{\Illuminate\Support\Facades\Auth::user()->name}}</strong></p>
                <p>از داشبورد حساب خود می توانید پروفایل خود را مشاهده کنید <a href="#">سفارشات اخیر</a>, مدیریت خود را
                    به <a href="#">آدرس های حمل و نقل و صورتحساب</a>, و <a href="#">گذرواژه و جزئیات حساب خود را ویرایش
                        کنید</a>.</p>

                <h3>سفارشات اخیر</h3>
                <div class="recent-orders-table table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>سفارش</th>
                            <th>تاریخ</th>
                            <th>وضعیت</th>
                            <th>مجموع</th>
                            <th>عملیات</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>#074145O5</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O6</td>
                            <td>18 دی 1398</td>
                            <td>در انتظار</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O7</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O5</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O6</td>
                            <td>18 دی 1398</td>
                            <td>در انتظار</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        <tr>
                            <td>#074145O7</td>
                            <td>18 دی 1398</td>
                            <td>تکمیل شده</td>
                            <td>49 تومان - 1 آیتم</td>
                            <td><a href="#" class="view-button">مشاهده</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->

@endsection
