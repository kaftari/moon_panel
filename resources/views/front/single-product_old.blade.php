﻿@extends('layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="{{route('index')}}">صفحه اصلی</a></li>
                    <li><a href="courses-2-columns-style-1.html">{{$post->main_category}}</a></li>
                    <li>{{$post->title}}</li>
                </ul>
                <h2>{{$post->title}}</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start Courses Details Area -->
    <section class="courses-details-area pt-100 pb-70">
        <div class="container">
            <div class="courses-details-header">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="courses-title">
                            <h2>{{$post->title}}</h2>
                            <p>{{$post->meta->en_name}}</p>
                        </div>

                        <div class="courses-meta">
                            <ul>
                                <li>
                                    <i class='bx bx-folder-open'></i>
                                    <span>دسته بندی</span>
                                    <a href="#">{{$post->main_category}}</a>
                                </li>
                                <li>
                                    <i class='bx bx-group'></i>
                                    <span>تعداد خرید</span>
                                    <a href="#">813,454</a>
                                </li>
                                <li>
                                    <i class='bx bx-calendar'></i>
                                    <span>آخرین آپدیت</span>
                                    <a href="#">{{$post->post_date}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="courses-price">
                            {{--                            <div class="courses-review">--}}
                            {{--                                <div class="review-stars">--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                    <i class='bx bxs-star'></i>--}}
                            {{--                                </div>--}}
                            {{--                                <span class="reviews-total d-inline-block"></span>--}}
                            {{--                            </div>--}}

{{--                            <div class="price"></div>--}}
{{--                            <a href="#" class="default-btn"><i class='bx bx-paper-plane icon-arrow before'></i><span--}}
{{--                                    class="label">پرداخت و دانلود</span><i--}}
{{--                                    class="bx bx-paper-plane icon-arrow after"></i></a>--}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8">
                    <div class="courses-details-image text-center">
                        <img src="{{$post->image}}" alt="image">
                    </div>

                    <div class="courses-details-desc" style="line-height: 3;">
{{--                        {!! explode('[vip]',$post->post_content)[0] !!}--}}
                        {!! explode('[vip]',nl2br($post->post_content))[0] !!}

{{--                        {!! nl2br(e($post->post_content)) !!}--}}
                        <h3>دانلود ویژه با اعتبار
                        </h3>

                        <div class="courses-author">
                            <div class="author-profile-header"></div>
                            <div class="author-profile">
                                <div class="author-profile-title">
                                    <img src="{{asset('assets/img/user1.jpg')}}" class="shadow-sm rounded-circle"
                                         alt="image">

                                    <div class="author-profile-title-details d-flex justify-content-between">
                                        <div class="author-profile-details">
                                            <h4></h4>
                                            <span class="d-block">دانلود مجموعه</span>
                                        </div>

                                        <div class="author-profile-raque-profile">
                                            <a href="{!! urldecode(str_replace("[/vip]", "",explode('[vip]',$post->post_content)[1])) !!}" class="d-inline-block">لینک دانلود</a>
{{--                                            <a href="{!! urldecode(str_replace("[/vip]", "",$vip)) !!}" class="d-inline-block">لینک دانلود</a>--}}
                                        </div>
                                    </div>
                                </div>
                                <p>رمز فایل ها : moonarch.ir</p>
                                {{--                                <p>لورم ایپسوم به سادگی متن ساختگی صنعت چاپ و تحریر است. لورم ایپسوم صنعت بوده است. لورم--}}
                                {{--                                    ایپسوم به سادگی متن ساختگی صنعت چاپ و تحریر است. لورم ایپسوم صنعت بوده است.</p>--}}
                            </div>
                        </div>


                    </div>

                    <div class="related-courses">
                        <h3>مطالب مرتبط</h3>

                        <div class="row">

                            @foreach($related_posts as $post)
                                <div class="col-lg-4 col-md-12">
                                    <div class="single-courses-box mb-30">
                                        <div class="courses-image">
                                            <a target="_blank" href="{{route('plusSingle',['slug'=>$post->slug])}}" class="d-block"><img
                                                    src="{{$post->image}}" alt="image"></a>

                                            <div class="courses-tag">
                                                <a href="#" class="d-block">{{$post->main_category}}</a>
                                            </div>
                                        </div>

                                        <div class="courses-content">
                                            <div class="course-author d-flex align-items-center">
                                                <img src="{{asset('assets/img/user1.jpg')}}" class="rounded-circle mr-2" alt="image">
                                                <span>{{$post->author->display_name}}</span>
                                            </div>

                                            <h3>
                                                <a target="_blank" href="{{route('plusSingle',['slug'=>$post->slug])}}" class="d-inline-block">
                                                    {{$post->title}}
                                                </a>
                                            </h3>

{{--                                            <div class="courses-rating">--}}
{{--                                                <div class="review-stars-rated">--}}
{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                    <i class='bx bxs-star'></i>--}}
{{--                                                </div>--}}

{{--                                                <div class="rating-total">--}}
{{--                                                    5.0 (1 امتیاز)--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                        </div>

{{--                                        <div class="courses-box-footer">--}}
{{--                                            <ul>--}}
{{--                                                <li class="students-number">--}}
{{--                                                    <i class='bx bx-user'></i> 10 دانشجو--}}
{{--                                                </li>--}}

{{--                                                <li class="courses-lesson">--}}
{{--                                                    <i class='bx bx-book-open'></i> 6 درس--}}
{{--                                                </li>--}}

{{--                                                <li class="courses-price">--}}
{{--                                                    رایگان--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="courses-sidebar-information">
                        <ul>
                            <li>
                                <span><i class='bx bx-group'></i> دانشجویان:</span>
                                10
                            </li>
                            <li>
                                <span><i class='bx bx-time'></i> مدت:</span>
                                1 هفته
                            </li>
                            <li>
                                <span><i class='bx bx-tachometer'></i> تلاش:</span>
                                2–5 ساعت در هفته
                            </li>
                            <li>
                                <span><i class='bx bxs-institution'></i> موسسه، نهاد:</span>
                                <a href="#" class="d-inline-block">تم فارست</a>
                            </li>
                            <li>
                                <span><i class='bx bxs-graduation'></i> موضوع:</span>
                                طراحی
                            </li>
                            <li>
                                <span><i class='bx bx-atom'></i> آزمونها:</span>
                                بله
                            </li>
                            <li>
                                <span><i class='bx bxs-badge-check'></i> سطح:</span>
                                صنعت
                            </li>
                            <li>
                                <span><i class='bx bx-support'></i> زبان:</span>
                                انگلیسی
                            </li>
                            <li>
                                <span><i class='bx bx-text'></i> زیرنویس ویدئو:</span>
                                انگلیسی
                            </li>
                            <li>
                                <span><i class='bx bx-certification'></i> تایید شده:</span>
                                بله
                            </li>
                        </ul>
                    </div>


                    <div class="courses-purchase-info">
                        <h4>آیا در این دوره برای کسب و کار یا تیم خود علاقه مند هستید؟</h4>
                        <p>برای تجارت ، کارمندان خود را در بیشترین درخواست ها آموزش دهید.</p>

                        <a href="#" class="d-inline-block">اکنون خرید کنید</a>
                        <a href="#" class="d-inline-block">درخواست اطلاعات</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Courses Details Area -->
@endsection
