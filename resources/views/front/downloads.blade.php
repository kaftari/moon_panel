﻿@extends('front.layouts.main')
@section('content')

    <!-- Start Page Title Area -->
    <div class="page-title-area item-bg3 jarallax" data-jarallax='{"speed": 0.3}'>
        <div class="container">
            <div class="page-title-content">
                <ul>
                    <li><a href="index.html">صفحه اصلی</a></li>
                    <li>حساب کاربری من</li>
                    <li>بارگیری ها</li>
                </ul>
                <h2>حساب کاربری من</h2>
            </div>
        </div>
    </div>
    <!-- End Page Title Area -->

    <!-- Start My Account Area -->
    <section class="my-account-area ptb-100">
        <div class="container">

            @include('front.partials.account.navbar')

            <div class="myAccount-content">
                <div class="downloads-table table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>نام آیتم</th>
                            <th>اتمام پشتیبانی</th>
                            <th>بارگیری</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="#">دسته رمان</a></td>
                            <td>13 دی 1398</td>
                            <td><a href="#" class="downloads-button">کلیک کنید</a></td>
                        </tr>
                        <tr>
                            <td><a href="#">کتاب داستان</a></td>
                            <td>هرگز</td>
                            <td><a href="#" class="downloads-button">کلیک کنید</a></td>
                        </tr>
                        <tr>
                            <td><a href="#">کتاب رمان</a></td>
                            <td>19 بهمن 1398</td>
                            <td><a href="#" class="downloads-button">کلیک کنید</a></td>
                        </tr>
                        <tr>
                            <td><a href="#">کتاب هوشمند</a></td>
                            <td>21 دی 1398</td>
                            <td><a href="#" class="downloads-button">کلیک کنید</a></td>
                        </tr>
                        <tr>
                            <td><a href="#">کتاب درسی</a></td>
                            <td>هرگز</td>
                            <td><a href="#" class="downloads-button">کلیک کنید</a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- End My Account Area -->
@endsection
