<div class="myAccount-navigation">
    <ul>
        <li><a href="{{route('dashboard.userAccount')}}" class="{{isset($active_account) ? 'active' : ''}}" ><i class='bx bxs-dashboard'></i> داشبورد</a></li>
        <li><a href="{{route('dashboard.userOrders')}}" class="{{isset($active_order) ? 'active' : ''}}" ><i class='bx bx-cart'></i> سفارشات</a></li>
        <li><a href="{{route('dashboard.userDownloads')}}" class="{{isset($active_downloads) ? 'active' : ''}}" ><i class='bx bx-download'></i> بارگیری ها</a></li>
        <li><a href="{{route('dashboard.userEditAddress')}}" class="{{isset($active_edit_address) ? 'active' : ''}}" ><i class='bx bx-home-alt'></i> آدرس ها</a></li>
        <li><a href="{{route('dashboard.userEditAccount')}}" class="{{isset($active_edit_aacount) ? 'active' : ''}}" ><i class='bx bx-edit'></i> جزئیات حساب</a></li>
        <li><a href="{{route('dashboard.logout')}}"><i class='bx bx-log-out'></i> خروج</a></li>
    </ul>
</div>
