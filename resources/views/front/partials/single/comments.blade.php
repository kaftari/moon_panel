<div class="tabs-item">
    <div class="products-details-tab-content">
        <div class="product-review-form">
            <h3>نظرات کاربران</h3>

            <div class="review-title">
                {{--<div class="rating">
                    <i class='bx bxs-star'></i>
                    <i class='bx bxs-star'></i>
                    <i class='bx bxs-star'></i>
                    <i class='bx bxs-star'></i>
                    <i class='bx bx-star'></i>
                </div>--}}
{{--                <p>تعداد نظرات {{ count($post->comments) }}</p>--}}

                {{--<a href="#" class="default-btn"><i
                        class='bx bx-plus icon-arrow before'></i><span
                        class="label">یک نظر بنویسید</span><i
                        class="bx bx-plus icon-arrow after"></i>
                </a>--}}
            </div>

            <div class="review-comments">

                @foreach($post->comments as $comment)
                    <div class="review-item">
                        {{--<div class="rating">
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bx-star'></i>
                        </div>--}}
                        <h3>{{$comment->subject}}</h3>
                        <span><strong>{{$comment->user['name']}}</strong> در <strong>{{\App\Helpers\Date::jalaliToday($comment->created_at)}}</strong></span>
                        <p>{!! $comment->message !!}</p>

                    </div>
                    @foreach($comment->replies as $reply)
                        <div class="review-item mr-3">
                            {{--<div class="rating">
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bxs-star'></i>
                                <i class='bx bx-star'></i>
                            </div>--}}
                            <h3>{{$reply->subject}}</h3>
                            <span><strong>{{$reply->user['name']}}</strong> در <strong>{{\App\Helpers\Date::jalaliToday($reply->created_at)}}</strong></span>
                            <p>{!! $reply->message !!}</p>

                        </div>
                    @endforeach
                @endforeach

            </div>

            <div class="review-form">
                @if(!\Illuminate\Support\Facades\Auth::check())
                    <h3><a href="{{route('login')}}" target="_blank">برای ارسال نظر ابتدا باید وارد حساب کاربری
                            خود شوید!</a></h3>
                @else

                    <h3>نظر بدهید</h3>

                    <form method="post" action="{{route('dashboard.sendComment',['post_id'=>$post->id])}}">
                        @csrf
                        <div class="row">

                            {{--<div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" id="name" name="name"
                                           placeholder="نام شما" class="form-control">
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <input type="email" id="email" name="email"
                                           placeholder="ایمیل شما" class="form-control">
                                </div>
                            </div>--}}

                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" id="review-title"
                                           name="subject" placeholder="موضوع نظر"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                                                    <textarea name="message" id="review-body"
                                                                              cols="30" rows="6"
                                                                              placeholder="پیام خود را بنویسید"
                                                                              class="form-control" required></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="default-btn"><i
                                        class='bx bx-paper-plane icon-arrow before'></i><span
                                        class="label">ارسال نظر</span><i
                                        class="bx bx-paper-plane icon-arrow after"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
