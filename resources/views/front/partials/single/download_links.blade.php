<div class="tabs-item">
    <div class="products-details-tab-content text-justify" style="line-height: 2">
        {!! $post->content !!}
        <div class="alertbox dlbox thingy">
            <div class="headline">
                <i class="fa fa-download"></i> دانلود مجموعه
                <span class="shine">از سرعت دانلود لذت ببرید!</span>
                <span class="premium badgak">
                                                        @if($post->post_cost == 'vip')
                        {{"دانلود ویژه با اعتبار"}}
                    @else
                        {{"رایگان"}}
                    @endif
                                                    </span>
            </div>

            <div class="dlboxmsg trs04">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    <a href="https://moonarch.ir/my-account/" title="حساب کاربری"
                       class="trs04">ورود / ثبت نام</a>
                    <a href="https://moonarch.ir/product/%d8%aa%da%a9%d8%b3%da%86%d8%b1-%da%a9%d8%a7%d8%b4%db%8c-38/"
                       title="حساب کاربری" class="trs04 grey">آموزش ثبت نام</a>
                </div>
            </div>

            <div class="links">
                @foreach($post->attachment as $key => $item)
                    <div class="plain">{{$item->title}}</div>
                    @foreach(json_decode($item->links) as $link)
                        @if($link != "")
                            @if ( $post->post_cost == 'vip' && !\Illuminate\Support\Facades\Auth::check() )
                                <a href="{{route('login')}}" class="a">
                                    @elseif ($post->post_cost == 'vip' && !\App\Helpers\Users::checkUserStatus())
                                        <a href="{{route('vipBuy')}}" class="a">
                                            @elseif($post->post_cost == 'free')
                                                <a href="{{'http://'.$link}}" class="a">
                                                    @endif
                                                    دریافت فایل
                                                    <span>
                    @php $link = explode('/',urldecode($link)); @endphp
                                                        {{end($link)}}
                </span>
                                                </a>
                            @endif
                            @endforeach
                            @endforeach
            </div>


            <div class="passandhelp cleaner row">
                <div class="col-md-3">
                    <button class="outline download_help_button full-width"><i
                            class="fa fa-question-circle"></i> راهنمای دانلود
                    </button>
                </div>
                <div class="col-md-4">
                    <button class="outline download_help_button full-width"><i
                            class="fa fa-exclamation-circle"></i> همه پارت هارا
                        دانلود کنید
                    </button>
                </div>
                <div class="col-md-5">
                    <div class="passwordbox">
                        <span><i class="fa fa-lock"></i> رمز فایل (تایپ کنید)</span>
                        <div class="passwordinput">moonarch.ir</div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
