<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>عمومی</h3>
        <ul class="nav side-menu">
            {{--<li><a><i class="fa fa-home"></i> خانه <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="index.html">داشبورد</a></li>
                    <li><a href="index2.html">داشبورد ۲</a></li>
                    <li><a href="index3.html">داشبورد ۳</a></li>
                </ul>
            </li>--}}
            <li><a><i class="fa fa-edit"></i>  نوشته ها <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{route('panel.admin.postIndex')}}">همه نوشته ها</a></li>
                    <li><a href="{{route('panel.admin.categoryCreate')}}">دسته ها</a></li>
                    <li><a href="{{route('panel.admin.creatorCreate')}}">سازنده ها</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i> مدیریت کاربران <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{route('panel.admin.usersIndex')}}">لیست کاربران</a></li>
                    <li><a href="{{route('panel.admin.usersVipIndex')}}">لیست کاربران ویژه</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-dollar"></i> مدیریت مالی <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{route('panel.admin.subscribePlan')}}">پلن های اشتراک</a></li>
{{--                    <li><a href="#">گزارش فروش اشتراک</a></li>--}}
                </ul>
            </li>
            {{--<li><a><i class="fa fa-bar-chart-o"></i> ارائه داده <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="chartjs.html">Chart JS</a></li>
                    <li><a href="chartjs2.html">Chart JS2</a></li>
                    <li><a href="morisjs.html">Moris JS</a></li>
                    <li><a href="echarts.html">ECharts</a></li>
                    <li><a href="other_charts.html">چارت های دیگر</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-clone"></i>طرح بندی <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="fixed_sidebar.html">نوار کناری ثابت</a></li>
                    <li><a href="fixed_footer.html">پاورقی ثابت</a></li>
                </ul>
            </li>--}}
        </ul>
    </div>

    {{--<div class="menu_section">
        <h3>به صورت زنده</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-bug"></i> صفحات اضافی <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="e_commerce.html">تجارت الکترونیک</a></li>
                    <li><a href="projects.html">پروژه ها</a></li>
                    <li><a href="project_detail.html">جزئیات پروژه</a></li>
                    <li><a href="contacts.html">اطلاعات تماس</a></li>
                    <li><a href="profile.html">نمایه</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-windows"></i> افزودنیهای پیشنهاد شده <span
                        class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="page_403.html">403 ارور</a></li>
                    <li><a href="page_404.html">404 ارور</a></li>
                    <li><a href="page_500.html">500 ارور</a></li>
                    <li><a href="plain_page.html">صفحه ساده</a></li>
                    <li><a href="login.html">صفحه ورود</a></li>
                    <li><a href="pricing_tables.html">جداول قیمت</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> منو چند سطحی <span
                        class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#level1_1">سطح یک</a>
                    <li><a>سطح یک<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">سطح دو</a>
                            </li>
                            <li><a href="#level2_1">سطح دو</a>
                            </li>
                            <li><a href="#level2_2">سطح دو</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#level1_2">سطح یک</a>
                    </li>
                </ul>
            </li>
            <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> صفحه مقصد <span
                        class="label label-success pull-left">به زودی</span></a></li>
        </ul>
    </div>--}}

</div>
