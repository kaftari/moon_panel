@extends('panel.layouts.master-panel')
@section('content')
    <div class="row">
        <div class="col-md-8 col-xs-12">

            <div class="x_panel">
                <div class="x_title">
                    <h2>ویرایش دسته

                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">تنظیمات 1</a>
                                </li>
                                <li><a href="#">تنظیمات 2</a>
                                </li>
                            </ul>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <!-- start form for validation -->
                    <form method="post" action="{{route('panel.admin.categoryPatch',['id'=>$category->id])}}"
                          id="demo-form" data-parsley-validate enctype="multipart/form-data">

                        @csrf
                        <label for="fullname"> نام <span class="required">*</span> :</label>
                        <input type="text" id="name" class="form-control" name="name"
                               value="{{$category->name}}" required/>

                        <br>

                        <label for="email">نامک :</label>
                        <input type="text" id="slug" class="form-control"
                               value="{{$category->slug}}" name="slug"/>

                        <br>

                        {{--<label for="message">توضیحات</label>
                        <textarea name="description" class="form-control">{{$category->description}}</textarea>

                        <br>

                        <label for="fullname"> تصویر :</label>
                        <input type="file" id="image" class="form-control" name="image"/>

                        <br>

                        <label for="heard">نوع دسته<span class="required">*</span>:</label>
                        <select id="type" name="type" class="form-control select2">

                            @foreach($category_types as $key => $type)

                                <option
                                    {{$category->type == $key ? 'selected' : ''}}  value="{{$type}}">{{$type}}</option>

                            @endforeach
                        </select>

                        <div></div>
                        <br>--}}

                        <label for="heard">دسته اصلی<span class="required">*</span>:</label>
                        <select id="parent_id" name="parent_id" class="form-control select2">

                            <option {{$category->parent_id == 0 ? 'selected' : ''}} value="0">مادر</option>
                            @foreach($categories as $item)
                                @if($item->id != $category->id)
                                    <option
                                        {{$category->parent_id == $item->id ? 'selected' : ''}}  value="{{$item->id}}">{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>

                        <div></div>
                        <br/>

                        <button type="submit" class="btn btn-primary">ثبت</button>

                    </form>
                    <!-- end form for validations -->

                </div>
            </div>


        </div>

        <div class="col-md-4 col-xs-12">

            <div class="x_panel">
                <div class="row">
                    <div class="col">
                        <div>
                            <div class="image view view-first">
                                <img height="280px" style=" width: 100%;display: block;"
                                     src="{{$category->getImagePath()}}"
                                     alt="image"/>
                                <div class="mask">
                                    <div class="tools tools-bottom">
                                        <a target="_blank" href="{{$category->getImagePath()}}"><i
                                                class="fa fa-link"></i></a>
                                        <a href="#"><i class="fa fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                            {{--<div class="caption">
                                <p>بارش برف و یخ برای جنوب</p>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endsection

    @section('css')
        <!-- select2 -->
            <link href="{{asset('panel/vendors/select2/dist/css/select2.css')}}" rel="stylesheet">
    @stop

    @section('js')
        <!-- CKEditor init -->
            <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>

            <!-- Select2 -->
            <script src="{{asset('panel/vendors/select2/dist/js/select2.js')}}"></script>

            <script>
                var route_prefix = "/filemanager";
            </script>

            <script>
                $('textarea[name=description]').ckeditor({
                    height: 250,
                    language: 'fa',
                    filebrowserImageBrowseUrl: route_prefix + '?type=Images',
                    filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
                    filebrowserBrowseUrl: route_prefix + '?type=Files',
                    filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
                });
            </script>

            <script>
                $(document).ready(function () {
                    $("#parent_id").select2();
                    $("#type").select2();
                });
            </script>
@stop

