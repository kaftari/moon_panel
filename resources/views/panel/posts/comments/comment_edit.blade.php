@extends('panel.layouts.master-panel')
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{  session('success') }}
                </div>
            @endif

            @if (session('error'))

                <div class="alert alert-danger">
                    <strong>Danger!</strong> {{session('error')}}
                </div>
            @endif

        </div>
    </div>

    <div class="row">
        <div class="x_content">
            <textarea class="form-control">{{$comment->message}}</textarea>
        </div>
    </div>


    <div class="row">

        <div class="x_panel">
            <div class="x_title">
                <h2>مشاهده و پاسخ به نظر
                    {{--<small>برای اعتبار سنجی کلیک کنید</small>--}}
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">تنظیمات 1</a>
                            </li>
                            <li><a href="#">تنظیمات 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form data-parsley-validate method="post"
                      action="{{route('panel.admin.commentReply',['id'=>$comment->id])}}"
                      enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" id="message" class="form-control" name="subject"
                           value="{{ ' پاسخ به '.  $comment->subject}}"
                           required/>

                    <textarea name="message" class="form-control"></textarea>
                    @error('message')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                    <br/>

                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        پاسخ
                    </button>
                </form>
            </div>
        </div>


    </div>

@endsection
