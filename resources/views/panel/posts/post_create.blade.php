@extends('panel.layouts.master-panel')
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{  session('success') }}
                </div>
            @endif

            @if (session('error'))

                <div class="alert alert-danger">
                    <strong>Danger!</strong> {{session('error')}}
                </div>
            @endif

        </div>
    </div>

    <form data-parsley-validate method="post" action="{{route('panel.admin.postStore')}}"
          enctype="multipart/form-data">
        @csrf
        <div class="row">

            <div class="col-md-8">

                <div class="x_panel">
                    <div class="x_title">
                        <h2>نوشته جدید
                            {{--<small>برای اعتبار سنجی کلیک کنید</small>--}}
                        </h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <!-- start form for validation -->


                        <label for="fullname">عنوان<span class="required">*</span></label>
                        <input type="text" id="title" class="form-control" name="title" required value="{{old('title')}}"/>
                        @error('title')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="fullname">آدرس اینترنتی<span class="required">*</span></label>
                        <input type="text" id="slug" class="form-control" name="slug" value="{{old('slug')}}" required />
                        @error('slug')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror
                        <br/>

                        <textarea name="description" class="form-control">{{old('description')}}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="fullname">شناسه ویدئو آپارات</label>
                        <input type="text" id="video_url" class="form-control" name="video_url" value="{{old('video_url')}}"/>
                        @error('video_url')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="fullname">عنوان انگلیسی محصول</label>
                        <input type="text" id="title_en" class="form-control" name="title_en" value="{{old('title_en')}}"/>
                        @error('title_en')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="fullname">حجم محصول به گیگابایت</label>
                        <input type="text" id="size" class="form-control" name="size" value="{{old('size')}}"/>
                        @error('size')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="fullname">نسخه محصول</label>
                        <input type="text" id="version" class="form-control" name="version" value="{{old('version')}}"/>
                        @error('version')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror


                    </div>
                </div>

            </div>

            <div class="col-md-4">
                <div class="x_panel">
                    <div class="x_title">
                        <button type="submit" class="btn btn-success" name="publish" value="1">
                            <i class="fa fa-save"></i>
                            انتشار
                        </button>
                        <button type="submit" class="btn btn-danger" name="publish" value="0">
                            <i class="fa fa-save"></i>
                            عدم انتشار
                        </button>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">تنظیمات 1</a>
                                    </li>
                                    <li><a href="#">تنظیمات 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <label for="heard"> دسته بندی <span class="required">*</span>:</label>
                        <select id="category_ids" class="form-control select2" name="category_ids[]" required multiple>
                            <option value="">انتخاب..</option>
                            @foreach($categories as  $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('category_ids')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <div></div>
                        <br>

                        <label for="heard"> سازنده <span class="required">*</span>:</label>
                        <select id="creator_id" class="form-control select2" name="creator_id">
                            <option value="">انتخاب..</option>
                            @foreach($creators as  $creator)
                                <option value="{{$creator->id}}">{{$creator->title}}</option>
                            @endforeach
                        </select>
                        @error('creator_id')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <div></div>
                        <br>

                       {{-- <label for="heard"> نوع پست <span class="required">*</span>:</label>
                        <select id="heard" class="form-control" name="post_type" required>
                            <option value="">انتخاب..</option>
                            <option value="object">آبجکت</option>
                            <option value="texture">تکستچر</option>
                            <option value="normal">معمولی</option>
                        </select>
                        @error('post_type')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>--}}

                        <label for="heard"> نوع پست <span class="required">*</span>:</label>
                        <select id="heard" name="post_cost" class="form-control" required>
                            <option value="">انتخاب..</option>
                            <option value="vip">ویژه</option>
                            <option value="free">رایگان</option>
                        </select>
                        @error('post_cost')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br/>

                        <label for="heard"> برچسب ها <span class="required"></span>:</label>
                        <input id="tags_1" type="text" class="tags form-control" name="tags" value="{{old('tags')}}"/>
                        <br>
                        @error('tags')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                        <label for="heard"> تصویر شاخص <span class="required">*</span>:</label>
                        <div class="input-group">
                              <span class="input-group-btn">
                                <a id="lfm" data-input="thumbnail" data-preview="holder"
                                   class="btn btn-primary text-white">
                                  <i class="fa fa-picture-o"></i> Choose
                                </a>
                              </span>
                            <input id="thumbnail" class="form-control" type="text" name="image" value="{{old('image')}}" required>
                        </div>
                        @error('image')
                        <span class="invalid-feedback required" role="alert">
                                                <strong class="required">{{ $message }}</strong>
                                            </span>
                        @enderror

                        <br>

                        <label for="heard"> تصاویر مرتبط<span class="required">*</span>:</label>
                        <div class="input-group">
                              <span class="input-group-btn">
                                <a id="lfm2" data-input="thumbnail2" data-preview="holder2"
                                   class="btn btn-primary text-white">
                                  <i class="fa fa-picture-o"></i> Choose
                                </a>
                              </span>
                            <input id="thumbnail2" class="form-control" type="text" name="images" value="{{old('images')}}">
                        </div>
                        @error('images')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror

                    <!-- end form for validations -->

                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <label for="fullname">لینک های دانلود<span class="required">*</span></label>
            <div class="table-responsive">
                <table class="table table-bordered" id="dynamic_field">

                    <button type="button" name="add" id="add" class="btn btn-success">اضافه کردن
                    </button>
                    <tr>
                        <td>
                            <input type="text" name="link_title[]" placeholder="عنوان لینک"
                                   class="form-control name_list"/>
                            <br>
                            <textarea cols="8" rows="4" type="text" name="link_download[]" placeholder="آدرس های دانلود"
                                      class="form-control"></textarea>
                        </td>

                    </tr>

                </table>
            </div>
        </div>

    </form>

@endsection

@section('css')
    <!-- Multi Select -->
    <link rel="stylesheet" type="text/css" href="{{asset('panel/build/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('panel/build/css/jquery.multiselect.css')}}"/>
    <link href="{{asset('panel/vendors/select2/dist/css/select2.css')}}" rel="stylesheet">
@stop
@section('js')

    {{--  <!-- CKEditor init -->
      <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js"></script>--}}

    <!-- jQuery Tags Input -->
    <script src="{{asset('panel')}}/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>

    <!-- Switchery -->
    <script src="{{asset('panel')}}/vendors/switchery/dist/switchery.min.js"></script>

    <!-- Select2 -->
    <script src="{{asset('panel/vendors/select2/dist/js/select2.js')}}"></script>

    <!-- Parsley -->
    <script src="{{asset('panel')}}/vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="{{asset('panel')}}/vendors/parsleyjs/dist/i18n/fa.js"></script>

    <!---Multi select-->
    <script src="{{asset('panel/build/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('panel/build/js/jquery.multiselect.js')}}"></script>

    <script>
        var route_prefix = "/filemanager";
    </script>

    <script>
        $(document).ready(function () {
            $("#category_ids").select2();
            $("#creator_id").select2();
        });
    </script>

    {{--<script>
        $('textarea[name=content]').ckeditor({
            height: 500,
            language: 'fa',
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>--}}

    <!-- TinyMCE init -->
    <script src="{{asset('panel')}}/custom/tinymce4/tinymce.min.js"></script>
    {{--    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>--}}
    <script>
        var editor_config = {
            path_absolute: "",
            selector: "textarea[name=description]",
            directionality: 'rtl',
            language_url: '{{asset('panel/custom/tinymce/langs/fa_IR.js')}}',
            plugins: [
                "link image"
            ],
            relative_urls: false,
            height: 300,
            file_browser_callback: function (field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>

    <script>
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
    </script>
    <script>
        $('#lfm').filemanager('image', {prefix: route_prefix});
        $('#lfm2').filemanager('image', {prefix: route_prefix});
    </script>



    <script type="text/javascript">
        $(document).ready(function () {

            /*var postURL = "<?php echo url('addmore'); ?>";*/
            let i = 1;

            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added">' +
                    '<td>' +
                    '<input type="text" name="link_title[]" placeholder="عنوان لینک" class="form-control name_list" />' +
                    '<br>' +
                    '<textarea cols="8" rows="4"  type="text" name="link_download[]" placeholder="آدرس های دانلود" class="form-control"></textarea>' +
                    '</td>' +
                    '<td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
            });


            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });

        });
    </script>

@stop
