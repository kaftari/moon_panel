@extends('panel.layouts.master-panel')
@section('content')

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">

            @if (session('success'))
                <div class="alert alert-success" role="alert">
                    {{  session('success') }}
                </div>
            @endif

            @if (session('error'))

                <div class="alert alert-danger">
                    <strong>Danger!</strong> {{session('error')}}
                </div>
            @endif

        </div>
    </div>

    <div class="col-md-12 col-xs-12">

        <div class="x_panel">
            <div class="x_title">
                <h2> کابر جدید

                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">تنظیمات 1</a>
                            </li>
                            <li><a href="#">تنظیمات 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <!-- start form for validation -->
                <form method="post" action="{{route('panel.admin.userStore')}}"
                      id="demo-form" data-parsley-validate enctype="multipart/form-data">

                    @csrf

                    <label for="fullname"> ایمیل <span class="required">*</span> :</label>
                    <input type="email" id="email" class="form-control" name="email" required/>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                    <br>

                    <label for="fullname"> نام کاربری <span class="required">*</span> :</label>
                    <input type="text" id="username" class="form-control" name="username" required/>
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                    <br>

                    <label for="email"> رمز عبور  <span class="required">*</span> :</label>
                    <input type="password" id="password" class="form-control" name="password"/>
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                    <br>

                    <label for="heard">نوع عضویت<span class="required">*</span>:</label>
                    <select id="user_status" name="user_status" class="form-control select2">
                        @foreach(\App\Models\User::user_status_const() as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>

                    <div></div>
                    <br/>

                    <label for="fullname"> دوره عضویت  :</label>
                    <input type="number" id="validity_duration" class="form-control" name="validity_duration" placeholder="در صورتی که کاربر ویژه است این مقدار را با عدد روز پر کنید"/>
                    @error('validity_duration')
                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                    @enderror
                    <br>

                    {{--<label for="fullname"> تصویر :</label>
                    <input type="file" id="image" class="form-control" name="image"/>

                    <br>--}}

                    <button type="submit" class="btn btn-primary">ثبت</button>

                </form>
                <!-- end form for validations -->

            </div>
        </div>


    </div>

@endsection

@section('css')
    <!-- Multi Select -->
    <link rel="stylesheet" type="text/css" href="{{asset('panel/build/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('panel/build/css/jquery.multiselect.css')}}"/>
    <link href="{{asset('panel/vendors/select2/dist/css/select2.css')}}" rel="stylesheet">
@stop
@section('js')

    <!-- jQuery Tags Input -->
    <script src="{{asset('panel')}}/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>

    <!-- Switchery -->
    <script src="{{asset('panel')}}/vendors/switchery/dist/switchery.min.js"></script>

    <!-- Select2 -->
    <script src="{{asset('panel/vendors/select2/dist/js/select2.js')}}"></script>

    <!-- Parsley -->
    <script src="{{asset('panel')}}/vendors/parsleyjs/dist/parsley.min.js"></script>
    <script src="{{asset('panel')}}/vendors/parsleyjs/dist/i18n/fa.js"></script>

    <!---Multi select-->
    <script src="{{asset('panel/build/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('panel/build/js/jquery.multiselect.js')}}"></script>

    <script>
        $(document).ready(function () {
            $("#category_ids").select2();
            $("#creator_id").select2();
        });
    </script>


@stop
