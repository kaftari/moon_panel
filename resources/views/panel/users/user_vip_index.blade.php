@extends('panel.layouts.master-panel')
@section('content')
    <div class="row">

        <div class="x_panel">
            <div class="x_title">
                <h2>کاربران ویژه
                    {{--<small>عناصر فرم های مختلف</small>--}}
                </h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">تنظیمات 1</a>
                            </li>
                            <li><a href="#">تنظیمات 2</a>
                            </li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div class="table-responsive">
                    <table id="users_table" class="table table-striped jambo_table"
                           data-href="{{route('panel.admin.usersVipJson')}}">
                        <thead>
                        <tr class="headings">
                            <th class="column-title text-center">نام کاربری</th>
                            <th class="column-title text-center">ایمیل</th>
                            <th class="column-title text-center">نوع کاربر</th>
                            <th class="column-title text-center">تاریخ عضویت</th>
                            <th class="column-title no-link last text-center"><span
                                    class="nobr">عملیات</span>
                            </th>

                        </tr>
                        </thead>

                        <tbody class="text-center">

                        </tbody>
                    </table>
                </div>


            </div>
        </div>

    </div>
@endsection

