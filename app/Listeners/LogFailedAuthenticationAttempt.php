<?php

namespace App\Listeners;

use App\Models\User;

use Illuminate\Auth\Events\Failed;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use MikeMcLin\WpPassword\Facades\WpPassword;

class LogFailedAuthenticationAttempt
{
    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        $user = User::where('username', $event->credentials['username'])->first();
        if ($user) {
            if (WpPassword::check($event->credentials['password'], $user->password)) {
                Auth::login($user);
                $user->password = Hash::make($event->credentials['password']);
                $user->save();
            }
        }
    }
}
