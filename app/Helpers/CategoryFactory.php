<?php


namespace App\Helpers;

use App\Models\Age;
use App\Models\Book;
use App\Models\Mark;
use App\Models\Verse;
use App\Models\VerseCategory;
use Illuminate\Support\Facades\DB;

class CategoryFactory
{
    public static function treeList($category)
    {
        return $category::orderByRaw('-title ASC')
            ->get()
            ->nest()
            ->setIndent('> ')
            ->listsFlattened('title');
    }

    public static function getCategoryById($id)
    {
        $category = VerseCategory::where('id',$id)->first();
        return $category ? $category : false;
    }

    public static function getRelatedVerseByMainSubjects($id)
    {
//        return Verse::where('id',$id)->first();
//        return DB::table('verses')
        return Verse::with('mark')
            ->whereJsonContains('main_subjects', $id)
            ->limit(2)
            ->get();

    }

    public static function getAgeById($id)
    {
        return Age::where('id',$id)->first();
    }

    public static function getBookById($id)
    {
        return Book::where('id',$id)->first();
    }

    public static function getMarkById($id)
    {
        return Mark::where('id',$id)->with('sura')->first();
    }
}
