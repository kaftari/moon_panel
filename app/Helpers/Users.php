<?php


namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Users
{
    public static function checkUserStatus()
    {
        if (Auth::check() && Auth::user()->user_status && Date::check_expire_date(Auth::user()->expire_at_vip))
            return true;

        return false;
    }

}
