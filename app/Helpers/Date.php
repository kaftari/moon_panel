<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Morilog\Jalali\CalendarUtils;
use Morilog\Jalali\Jalalian;

class Date
{
    public static function jalaliToGregorian($date)
    {
        $dt = explode('/', $date);

        return implode('-', CalendarUtils::toGregorian($dt[0], $dt[1], $dt[2]));
    }

    public static function gregorianTojalali($date)
    {
        $dt = Carbon::parse($date);
        $dt = explode('-', $dt->format('Y-m-d'));

        return implode('/', CalendarUtils::toJalali($dt[0], $dt[1], $dt[2]));
    }

    /*
     * return true if date between start and end.
     * all date must be gregorian
     */
    public static function betweenTwoDate($startDate, $endDate, $date)
    {
        if (strtotime($startDate) <= strtotime($date) && strtotime($endDate) >= strtotime($date))
            return true;

        return false;
    }

    public static function diffInMinute($time_start = null, $time_end = null)
    {
        if (is_null($time_start) || is_null($time_end))
            return 0;

        $start = new Carbon($time_start);
        $end = new Carbon($time_end);


        return $start->diffInMinutes($end);
    }

    public static function jalaliToday($date)
    {
        $date = Carbon::parse($date)->timestamp;
        $date = Jalalian::forge($date)->format('%A, %d %B %y');

        return $date;
    }

    public static function convertDateNumbersToPersian($date)
    {
        return CalendarUtils::convertNumbers($date);
    }

    public static function calculate_expire_date($date, $days)
    {
        $date = Carbon::parse($date);

        return $date->addDays($days)->format('Y-m-d H:i:s');
    }

    public static function check_expire_date($date)
    {
        if ($date >= Carbon::now())
            return true;

        return false;
    }

    public static function finding_days_between_two_dates($from = null, $to = null)
    {
        if (!$to)
            $to = Carbon::now();

        if (!$from)
            $from = Auth::user()->expire_at_vip;

        $from = new \DateTime($from);

        $interval = $from->diff($to);
        return $interval->format('%a');//now do whatever you like with $days

    }

    public static function local_date_time()
    {
        //Iran is 3.30 behind of utc date time
        return Carbon::now()->addHours(3)->addMinutes(30);

    }

}
