<?php


namespace App\Helpers;


class Languages
{
    public static function list()
    {
        return [
            'fa' => 'فارسی',
            'en' => 'انگلیسی',
            'gr' => 'آلمانی'
        ];
    }

    public static function findByKey($key)
    {
        return self::list()[$key];
    }
}
