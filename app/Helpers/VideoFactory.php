<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class VideoFactory extends Facade
{
    public static function upload($video,$path,$videoName)
    {
        if (!File::isDirectory($path))
            File::makeDirectory($path,0777, true, true);

        $video->move($path, $videoName);
    }
}
