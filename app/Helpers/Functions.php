<?php

namespace App\Helpers;

class Functions
{
    public static function generateUniqueCode()
    {
        $comps = explode(' ', microtime());
        $date = sprintf('%d%03d', $comps[1], $comps[0] * 1000);

        return $date;
    }

    public static function genderTranslator($gender)
    {
        if ($gender == 1)
            return 'آقا';
        elseif ($gender == 2)
            return 'خانم';
        return 'مختلط';
    }

    public static function branchTranslator($branch)
    {
        if ($branch == 1)
            return 'مرزداران';
        return 'فرمانیه';
    }

    public static function separateNumber($number)
    {
        return number_format($number);
    }

    public static function visibleTranslator($visible_id)
    {
        if ($visible_id == 1)
            return 'مشاهده شده';
        return 'مشاهده نشده';
    }
}
