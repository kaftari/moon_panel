<?php

namespace App\Models;

use App\Helpers\ImageFactory;
use App\Models\Attachment;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements Viewable
{
    use HasFactory, InteractsWithViews;

    private $image_thumb_path;
    private $image_path;

    public function __construct()
    {
        $this->image_thumb_path = '/storage/photos/1/thumbs/';
        $this->image_path = '/storage/photos/1/';
    }

    #region Relations

    public function comments()
    {
        return $this->hasMany(Comment::class)->orderByDesc('id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function attachment()
    {
        return $this->hasMany(Attachment::class);
    }

    public function images()
    {
        return $this->hasMany(PostImage::class);
    }

    public function creator()
    {
        return $this->belongsTo(Creator::class);
    }

    #endregion

    #region Functions
    public static function post_types()
    {
        return [
            'object' => 'آبجکت',
            'texture' => 'تکستچر',
            'normal' => 'معمولی',
        ];
    }

    public static function post_costs()
    {
        return [
            'vip' => "ویژه",
            'free' => "رایگان",
        ];
    }

    public function getImageThumb($image = null)
    {
        if (!$image)
            $image = $this->image;

        return $this->image_thumb_path . ImageFactory::getFileNameFromUrl($image);
    }

    public function getImage($image = null)
    {
        if (!$image)
            $image = $this->image;

        return $this->image_path . ImageFactory::getFileNameFromUrl($image);
    }


    #endregion

}
