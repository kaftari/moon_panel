<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    use HasFactory;

    #region Relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function replies()
    {
        return $this->hasMany(Comment::class,'parent_id','id');
    }
    #endregion

    #region functions
    public static function unapproved()
    {
        return Comment::where('status',0)->where('user_id','!=',Auth::user()->id)->count();
    }

    #endregion
}
