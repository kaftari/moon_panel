<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    private $image_path;

    public function __construct()
    {
        $this->image_path = asset('uploads/categories/images');
    }

    #region Relations
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    #endregion

    #region Functions
    public function getImagePath($image = null): string
    {
        return $image ? $this->image_path.'/' . $image : $this->image_path.'/' . $this->image;
    }

    public static function category_type(): array
    {
        return [
            'artist' => 'آرتیست',
            'genre' => 'ژانر',
            'musical_instrument' => 'ساز موسیقی'
        ];
    }

    public function category_type_translate($key): string
    {
        return self::category_type()[$key];
    }
    #endregion


}
