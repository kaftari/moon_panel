<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','detail','validity_duration','expire_at','created_at','updated_at','detail'];

    #region Relations
    public function user()
    {
        return $this->belongsTo(User::class)->orderByDesc('id');
    }
    #endregion
}
