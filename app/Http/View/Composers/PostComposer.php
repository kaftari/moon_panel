<?php


namespace App\Http\View\Composers;


use App\Models\Category;
use App\Models\Creator;
use Illuminate\View\View;

class PostComposer
{
    public function compose(View $view)
    {
        $view->with([
            'categories' => Category::all(),
            'creators' => Creator::all(),
        ]);
    }
}
