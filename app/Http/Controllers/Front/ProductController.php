<?php

namespace App\Http\Controllers\Front;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function single($slug)
    {
        $post = Post::where('slug', $slug)->with(['attachment', 'images', 'creator', 'categories', 'comments' => function ($query) {
            $query->where('status', 1)
                ->where('parent_id', null);
        }])->first();

        $categories = [];
        foreach ($post->categories as $category) {
            $categories [] = $category->id;
        }


        $related_posts = Post::where('id','!=',$post->id)->with(['categories' => function ($query) use ($categories) {
            $query->whereIn('category_id', $categories);
        }])
            ->orderByDesc('id')
            ->limit(4)
            ->get();

        views($post)->record();

        // Cache for 3600 seconds
        // views($post)->remember(3600)->count();
        // More detail  : https://github.com/cyrildewit/eloquent-viewable#caching-view-counts

        return view('front.single-product', [
            'post' => $post,
            'tags' => json_decode($post->tags, true),
            'categories' => $post->categories->toArray(),
            'post_views' => views($post)->unique()->count(),
            'related_posts' => $related_posts
        ]);
    }

    public function send_comment(Request $request)
    {
        if (!Auth::check())
            return redirect()->back()->with('error', 'برای انجام این عملیات شما باید وارد حساب کاربری خود شوید');

        $request->validate([
            'subject' => 'required',
            'message' => 'required',
            'post_id' => 'required'
        ]);


        try {
        $comment = new Comment();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $request->get('post_id');
        $comment->subject = $request->subject;
        $comment->message = $request->message;
        $comment->created_at = Date::local_date_time();
        $comment->updated_at = Date::local_date_time();
        $comment->save();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد!');
        } catch (\Exception $exception) {
        return redirect()->back()->with('error', $exception->getMessage());
        }

    }
}
