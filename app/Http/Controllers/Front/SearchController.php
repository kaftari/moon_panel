<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    //
    public function search(Request $request)
    {
        $posts = Post::where('tags','like',"%{$request->get('search')}%")->paginate(20);
        return view('front.search',[
            'posts' => $posts
        ]);
//        $post = Post::whereJsonContains('tags',$request->get('search'))->get();
//        dd($post);
    }
}
