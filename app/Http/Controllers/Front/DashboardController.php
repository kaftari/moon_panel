<?php

namespace App\Http\Controllers\Front;

use App\Helpers\Date;
use App\Helpers\Users;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function dashboard()
    {
        return view('front.user-account',[
            'active_account' => true
        ]);
    }

    public function orders()
    {
        return view('front.orders',[
            'active_order' => true
        ]);
    }

    public function downloads()
    {
        return view('front.downloads',[
            'active_downloads' => true
        ]);
    }

    public function editAddress()
    {
        return view('front.edit-address',[
            'active_edit_address' => true
        ]);
    }

    public function editAccount()
    {
        return view('front.edit-account',[
            'active_edit_account' => true
        ]);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
