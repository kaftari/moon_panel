<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\PaymentPlan;
use App\Models\Post;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::where('publish', 1)->orderByDesc('id')->paginate(16);
        return view('front.index', [
            'posts' => $posts,
        ]);
    }

    public function vip_buy()
    {
        $plans = PaymentPlan::all();

        return view('front.buy_vip', [
            'plans' => $plans
        ]);
    }
}
