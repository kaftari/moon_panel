<?php

namespace App\Http\Controllers\Front;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Models\PaymentPlan;
use App\Models\User;
use App\Models\UserPayment;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;


class PaymentController extends Controller
{

    public function payment(Request $request)
    {
        if (!Auth::check())
            return redirect()->route('login');

        $plan = PaymentPlan::where('id', $request->get('plan'))->first();
        if (!$plan)
            return redirect()->route('vipBuy')->with('error', 'پلن مورد نظر در سیستم یافت نشد');


        $amount = intval($plan->discount);
//        $amount = 100;
        $days = $plan->days;

        $invoice = (new Invoice)->amount($amount);

        return Payment::purchase($invoice, function ($driver, $transactionId) use ($amount, $days) {

            UserPayment::create([
                'user_id' => Auth::user()->id,
                'validity_duration' => $days,
                'detail' => json_encode(['gateway_payment' => 'zarinpal', 'amount' => $amount, 'transaction_id' => $transactionId, 'status' => 0]),
                'expire_at' => Carbon::now()->addHours(3)->addMinutes(30),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        })->pay()->render();
    }

    public function verify(Request $request)
    {
        $user_payment = UserPayment::whereJsonContains('detail->transaction_id', $request->get('Authority'))->first();

        if (!$user_payment)
            return redirect()->route('vipBuy')->with('error', 'تراکنشی با این مشخصات در سیستم ثبت نشده است. لطفا با مدیر سایت تماس بگیرید!');

        $detail_payment = json_decode($user_payment->detail);

        try {

            $receipt = Payment::amount($detail_payment->amount)->transactionId($request->get('Authority'))->verify();

            // You can show payment referenceId to the user.

            $expire_date = Date::calculate_expire_date(Carbon::now(), $detail_payment->validity_duration);
            $user_payment->expire_at = $expire_date;
            $user_payment->detail = json_encode(['gateway_payment' => 'zarinpal', 'amount' => $detail_payment->amount,
                'transaction_id' => $detail_payment->transaction_id, 'status' => 1]);
            $user_payment->save();

            $user = User::where('id', Auth::user()->id)->first();
            $user->user_status = 1;
            $user->expire_at_vip = $expire_date;
            $user->save();

            return redirect()->route('vipBuy')->with('success', 'شماره پیگیری : ' . $receipt->getReferenceId());

        } catch (InvalidPaymentException $exception) {
            /**
             * when payment is not verified, it will throw an exception.
             * We can catch the exception to handle invalid payments.
             * getMessage method, returns a suitable message that can be used in user interface.
             **/
            return redirect()->route('vipBuy')->with('error', $exception->getMessage());
        }
    }
}
