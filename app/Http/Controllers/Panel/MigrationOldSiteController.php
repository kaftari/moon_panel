<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\UserPayment;
use App\Models\OldPost;
use App\Models\OldUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MigrationOldSiteController extends Controller
{
    public function __construct()
    {
        set_time_limit(8000000);
    }

    public function user_migrate()
    {

//        $buy_posts = OldPost::where('post_type','dpb_payment')->where('post_status','publish')->get();
        $users = OldUser::all();

        foreach ($users as $old_user) {

            try {
                $user = new User();
                $user->email = $old_user->user_email;
                $user->password = $old_user->user_pass;
                $user->username = $old_user->user_login;
                $user->name = $old_user->user_nicename;
                $user->date_register = $old_user->user_registered;
                $user->user_status = 0;
                $user->save();
            }
            catch (\Exception $exception){
                continue;
                dump($old_user->user_email);
            }

        }



       /* foreach ($buy_posts as $post){
            $old_user = OldUser::where('ID',$post->post_author)->first();

            if ($old_user){

                $exist_user = User::where('username',$old_user->user_login)->first();

                if ($exist_user){
                    $user_payment = new UserPayment();
                    $user_payment->user_id = $exist_user->id;
                    $user_payment->old_detail = $post->post_content;
                    $user_payment->expire_at = $this->calculate_expire_date($post->post_date,$post->post_title);
                    $user_payment->save();
                }

                else
                {
                    $user = new User();
                    $user->email = $old_user->user_email;
                    $user->password = $old_user->user_pass;
                    $user->username = $old_user->user_login;
                    $user->name = $old_user->user_nicename;
                    $user->date_register = $old_user->user_registered;
                    $user->user_status = 1;
                    $user->expire_at_vip = $this->calculate_expire_date($post->post_date,$post->post_title);
                    $user->save();

                    $user_payment = new UserPayment();
                    $user_payment->user_id = $user->id;
                    $user_payment->old_detail = $post->post_content;
                    $user_payment->expire_at = $this->calculate_expire_date($post->post_date,$post->post_title);
                    $user_payment->save();

                }

            }
        }*/
    }

    public function get_days_from_subscribe_string($str)
    {
        $str = explode('روز',explode('-',$str)[0])[0];

        return str_replace( array(":", "", " "), '', $str);
    }

    public function calculate_expire_date($date,$str)
    {
        $days = $this->get_days_from_subscribe_string($str);
        $date = Carbon::parse($date);
        return $date->addDays($days)->format('Y-m-d H:i:s');
    }

}
