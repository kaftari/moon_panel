<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\PaymentPlan;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class SubscribePlanController extends Controller
{
    public function json()
    {
        $plans = PaymentPlan::query()->orderByDesc('id');

        return Datatables::of($plans)
            ->addIndexColumn()
            ->addColumn('action', function ($plan) {
                $btn = '<a href="' . route('panel.admin.subscribePlanEdit', ['id' => $plan->id]) . '" class="edit btn btn-primary btn-sm">ویرایش</a>' .
                    '<a href="' . route('panel.admin.subscribePlanDelete', ['id' => $plan->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function plans()
    {
        return view('panel.financial.subscribe_plans.plan_create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'days' => 'required',
            'price' => 'required'
        ]);

        $plan = new PaymentPlan();
        $plan->days = $request->days;
        $plan->price = $request->price;
        $plan->discount = $request->discount > 0 ? $request->discount : $request->price;
        $plan->period = 'day';
        $plan->save();

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد');

    }

    public function edit(Request $request)
    {
        $plan = PaymentPlan::findOrFail($request->get('id'));

        return view('panel.financial.subscribe_plans.plan_edit',[
            'plan' => $plan
        ]);
    }

    public function patch(Request $request)
    {
        $request->validate([
            'days' => 'required',
            'price' => 'required'
        ]);

        $plan =  PaymentPlan::findOrFail($request->get('id'));
        $plan->days = $request->days;
        $plan->price = $request->price;
        $plan->discount = $request->discount > 0 ? $request->discount : $request->price;
        $plan->period = 'day';
        $plan->save();

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
    }

    public function delete(Request $request)
    {
        PaymentPlan::destroy($request->get('id'));

        return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
    }
}
