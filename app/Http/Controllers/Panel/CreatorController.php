<?php

namespace App\Http\Controllers\Panel;

use App\Helpers\Date;
use App\Helpers\ImageFactory;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Creator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CreatorController extends Controller
{

    public function json()
    {
        $creators = Creator::all()->sortByDesc('id');

        return Datatables::of($creators)
            ->addIndexColumn()
            ->addColumn('action', function ($creator) {
                $btn = '<a href="' . route('panel.admin.creatorEdit', ['id' => $creator->id]) . '" class="edit btn btn-primary btn-sm">ویرایش</a>' .
                    '<a href="' . route('panel.admin.creatorDelete', ['id' => $creator->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->editColumn('post_count', function ($creator) {
                return is_array($creator->posts) ? count($creator->posts) : 0;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('panel.posts.creator.creator_create');
    }

    public function store(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'slug' => 'required',
        ]);


        try {

            $creator = new Creator();
            $creator->title = $request->name;
            $creator->slug = $request->slug;
            $creator->description = $request->description;
            $creator->save();

            return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
        }
        catch (\Exception $exception)
        {
            return redirect()->back()->with('error',$exception->getMessage());
        }

    }

    public function edit($id)
    {
        return view('panel.posts.creator.creator_edit',[
            'creator' => Creator::findOrFail($id),
//            'category_types' => Category::category_type(),
        ]);
    }

    public function patch(Request $request,$id)
    {

        $request->validate([
            'name' => 'required',
            'slug' => 'required'
        ]);

        try
        {
            $category = Creator::findOrFail($id);
            $category->title = $request->name;
            $category->description = $request->description;
            $category->slug = $request->slug;

            $category->save();

            return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
        }
        catch (\Exception $exception)
        {
            return  redirect()->back()->with('error',$exception->getMessage());
        }

    }
}
