<?php

namespace App\Http\Controllers\Panel;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class CommentController extends Controller
{
    public function json()
    {
        $comments = Comment::where('user_id','!=',Auth::user()->id)->orderByDesc('id')->get();

        return Datatables::of($comments)
            ->addIndexColumn()
            ->addColumn('action', function ($comment) {
                $btn = '<a href="' . route('panel.admin.commentEdit', ['id' => $comment->id]) . '" class="edit btn btn-success btn-sm">پاسخ</a>' .
                    '<a href="' . route('panel.admin.commentApprove', ['id' => $comment->id]) . '" class="edit btn btn-primary btn-sm">تایید</a>' .
                    '<a href="' . route('panel.admin.commentDelete', ['id' => $comment->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->editColumn('user_id', function ($comment) {
                return $comment->user['name'];
            })
            ->editColumn('post_id', function ($comment) {
                return $comment->post['title'];
            })
            ->editColumn('created_at', function ($comment) {
                return Date::gregorianTojalali($comment->created_at);
            })
            ->editColumn('created_at', function ($comment) {
                return Date::gregorianTojalali($comment->created_at);
            })
            ->editColumn('status', function ($comment) {
                return $comment->status == 0 ? 'تایید نشده' : 'تایید شده';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function index()
    {
        return view('panel.posts.comments.comment_index');
    }

    public function edit(Request $request)
    {
        $comment = Comment::findOrFail($request->get('id'));

        return view('panel.posts.comments.comment_edit', [
            'comment' => $comment
        ]);
    }

    public function reply(Request $request)
    {
        $request->validate([
            'message' => 'required'
        ]);

        try {
            $id = $request->get('id');

            $comment = Comment::findOrFail($id);
            $comment->status = 1;
            $comment->save();

            $reply_comment = new Comment();
            $reply_comment->parent_id = $id;
            $reply_comment->user_id = Auth::user()->id;
            $reply_comment->post_id = $comment->post_id;
            $reply_comment->message = $request->message;
            $reply_comment->subject = $request->subject;
            $reply_comment->status = 1;
            $reply_comment->created_at = Date::local_date_time();

            $reply_comment->save();

            return redirect()->route('panel.admin.commentIndex')->with('success', 'عملیات با موفقیت انجام شد');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }


    }

    public function approve(Request $request)
    {
        try {
            $comment = Comment::findOrFail($request->get('id'));
            $comment->status = 1;
            $comment->save();

            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function delete(Request $request)
    {
        if (Comment::destroy($request->get('id')))
            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');

        return redirect()->back()->with('error','عملیات موفقیت آمیز نبود');

    }
}
