<?php

namespace App\Http\Controllers\Panel;

use App\Helpers\Date;
use App\Http\Controllers\Controller;
use App\Models\UserPayment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    public function json()
    {
        $users = User::query()->orderByDesc('date_register');

        return Datatables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                $btn = '<a href="' . route('panel.admin.userEdit', ['id' => $user->id]) . '" class="edit btn btn-primary btn-sm">ویرایش</a>';

                if ($user->id != Auth::user()->id)
                    $btn .= '<a href="' . route('panel.admin.userDelete', ['id' => $user->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->editColumn('user_status', function ($user) {
                return $user->user_status == 1 ? 'ویژه' : 'معمولی';
            })
            ->editColumn('date_register', function ($user) {
                return Date::gregorianTojalali($user->date_register);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function json_vip()
    {
        $users = User::query()->where('user_status',1)->orderByDesc('date_register');

        return Datatables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($user) {
                $btn = '<a href="' . route('panel.admin.userRenew', ['id' => $user->id]) . '" class="edit btn btn-primary btn-sm">تمدید</a>';

                if ($user->id != Auth::user()->id)
                    $btn .= '<a href="' . route('panel.admin.userDelete', ['id' => $user->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->editColumn('user_status', function ($user) {
                return $user->user_status == 1 ? 'ویژه' : 'معمولی';
            })
            ->editColumn('date_register', function ($user) {
                return Date::gregorianTojalali($user->date_register);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function index()
    {
        return view('panel.users.user_index');
    }

    public function create()
    {
        return view('panel.users.user_create');
    }

    public function store(Request $request)
    {
        try {

            $expire_at = $request->user_status == 1 ? Date::calculate_expire_date(Carbon::now(), $request->validity_duration) : Carbon::now();

            $user = new User();
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->name = $request->username;
            $user->user_status = $request->user_status;
            $user->date_register = Carbon::now();
            $user->expire_at_vip = $expire_at;
            $user->save();

            if ($request->user_status) {
                $user_payment = new UserPayment();
                $user_payment->user_id = $user->id;
                $user_payment->expire_at = $expire_at;
                $user_payment->validity_duration = $request->validity_duration;
                $user_payment->save();
            }

            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function edit(Request $request)
    {
        $user = User::where('id',$request->get('id'))->with('payments')->first();

        return view('panel.users.user_edit',[
            'user' => $user
        ]);
    }

    public function patch(Request $request)
    {
        try {

            $user =  User::findOrFail($request->get('id'));
            $user->username = $request->username;
            $user->email = $request->email;
            $user->user_status = $request->user_status;
            $user->save();

            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }

    public function index_user_vip()
    {
        return view('panel.users.user_vip_index');
    }

    public function renew(Request $request)
    {
        $user_id = $request->get('id');
//        $user_payment = UserPayment::where('expire_at','>=',$user_id)->with('payments')->first();
        $user_with_payments = User::
        select('user_payments.expire_at','user_payments.validity_duration','user_payments.id as payment_id'
            ,'users.id as user_id','users.user_status','users.expire_at_vip')
            ->where('users.id',$request->get('id'))
            ->leftJoin('user_payments',function ($join) use ($user_id){
                $join->on('user_payments.user_id','=','users.id')
                    ->where('user_payments.user_id',$user_id)
                    ->where('user_payments.expire_at','>=',Carbon::now());
            })
            ->first();


        return view('panel.users.user_renew_subscription',[
            'user' => $user_with_payments,
        ]);
    }

    public function patch_renew(Request $request)
    {
        try {


            DB::beginTransaction();
            $expire_at = Date::calculate_expire_date(Carbon::now(), $request->validity_duration);
            $user = User::where('id',$request->get('id'))->first();
            $user->expire_at_vip = $expire_at;
            $user->save();

            $user_payment  = new UserPayment();
            $user_payment->expire_at = $expire_at;
            $user_payment->user_id = $request->get('id');
            $user_payment->validity_duration = $request->validity_duration;
            $user_payment->detail = json_encode(['gateway_payment'=>'admin','geteway_detail'=>null]);
            $user_payment->save();

            DB::commit();

            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->with('error', $exception->getMessage());
        }
    }
}
