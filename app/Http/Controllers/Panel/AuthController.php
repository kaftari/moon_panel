<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        if (!Auth::check())
            return view('panel.login');

        return redirect()->route('panel.index');
    }

    public function login_perform(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])) {
            return redirect()->route('panel.index');
        }

        return redirect()->back()->with('error', 'نام کاربری یا رمز عبور اشتباه است!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('panelLogin');
    }

    public function dashboard_login()
    {
        if (!Auth::check())
            return view('front.login');

        return redirect()->route('dashboard.userAccount');
    }

    public function dashboard_login_perform(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ])) {

            return redirect()->route('dashboard.userAccount');
        }

        return redirect()->back()->with('error', 'نام کاربری یا رمز عبور اشتباه است!');
    }
}
