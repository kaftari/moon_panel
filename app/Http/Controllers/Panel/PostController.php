<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use App\Models\Post;
use App\Models\PostImage;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PostController extends Controller
{
    public function json()
    {
        $posts = Post::all()->sortByDesc('id');

        return Datatables::of($posts)
            ->addIndexColumn()
            ->addColumn('action', function ($post) {
                $btn = '<a href="' . route('panel.admin.postEdit', ['id' => $post->id]) . '" class="edit btn btn-primary btn-sm">ویرایش</a>' .
                    '<a href="' . route('panel.admin.postReplicate', ['id' => $post->id]) . '" class="edit btn btn-primary btn-sm">کپی</a>' .
                    '<a href="' . route('panel.admin.postDelete', ['id' => $post->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function index()
    {
        return view('panel.posts.post_index');
    }

    public function create()
    {
        return view('panel.posts.post_create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
//            'post_type' => 'required',
            'post_cost' => 'required',
            'image' => 'required',
            'category_ids' => 'required',
        ]);

        $post = new Post();
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->content = $request->description;
//        $post->post_type = $request->post_type;
        $post->post_cost = $request->post_cost;
        $post->creator_id = $request->creator_id;
        $post->video_url = $request->video_url;
        $post->size = $request->size;
        $post->version = $request->version;
        $post->title_en = $request->title_en;
        $post->image = $request->image;
        $post->tags = $request->tags ? json_encode(explode(',',$request->tags)) : json_encode([]);
        $post->publish = $request->publish;

        $post->save();

        $post->categories()->attach($request->category_ids);

        if ($request->images) {
            $images = explode(',', $request->images);

            foreach ($images as $image) {
                $post_image = new PostImage();
                $post_image->post_id = $post->id;
                $post_image->image = $image;
                $post_image->save();
            }

        }

        foreach ($request->link_title as $key => $item) {
            $links = explode('http://', $request->link_download[$key]);
            $post_attachment = new Attachment();
            $post_attachment->post_id = $post->id;
            $post_attachment->title = $item;
            $post_attachment->links = json_encode($links);
            $post_attachment->save();
        }

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
    }

    public function edit(Request $request)
    {
        $post = Post::where('id', $request->get('id'))->with(['attachment','images'])->first();

        $post_categories = [];

        foreach ($post->categories as $post_category) {
            $post_categories[] = $post_category->id;

        }

        return view('panel.posts.post_edit', [
            'post' => $post,
            'post_categories' => $post_categories
        ]);
    }

    public function patch(Request $request,$post_id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
//            'post_type' => 'required',
            'post_cost' => 'required',
            'image' => 'required',
            'category_ids' => 'required',
        ]);

        $post = Post::findOrFail($post_id);
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->content = $request->description;
//        $post->post_type = $request->post_type;
        $post->post_cost = $request->post_cost;
        $post->video_url = $request->video_url;
        $post->size = $request->size;
        $post->version = $request->version;
        $post->image = $request->image;
        $post->title_en = $request->title_en;
        $post->tags = $request->tags ? json_encode(explode(',',$request->tags)) : json_encode([]);
        $post->creator_id = $request->creator_id;
        $post->publish = $request->publish;

        $post->save();

        $post->categories()->sync($request->category_ids);

        if ($request->images) {
            $images = explode(',', $request->images);

            foreach ($images as $image) {
                $post_image = new PostImage();
                $post_image->post_id = $post->id;
                $post_image->image = $image;
                $post_image->save();
            }
        }

        Attachment::where('post_id',$post->id)->delete();

        foreach ($request->link_title as $key => $item) {
            $links = explode('http://', $request->link_download[$key]);
            $post_attachment = new Attachment();
            $post_attachment->post_id = $post->id;
            $post_attachment->title = $item;
            $post_attachment->links = json_encode($links);
            $post_attachment->save();
        }

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
    }

    public function replicate(Request $request)
    {
        $post = Post::where('id', $request->get('id'))->with(['attachment','images'])->first();

        $post_categories = [];

        foreach ($post->categories as $post_category) {
            $post_categories[] = $post_category->id;

        }

        return view('panel.posts.post_replicate', [
            'post' => $post,
            'post_categories' => $post_categories
        ]);
    }

    public function replicate_perform(Request $request,$post_id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
//            'post_type' => 'required',
            'post_cost' => 'required',
            'image' => 'required',
            'category_ids' => 'required',
        ]);

        try {
            $post = Post::findOrFail($post_id);

            $post_new = $post->replicate();

            $post_new->title = $request->title;
            $post_new->slug = $request->slug;
            $post_new->content = $request->description;
            $post_new->post_cost = $request->post_cost;
            $post_new->video_url = $request->video_url;
            $post_new->size = $request->size;
            $post_new->version = $request->version;
            $post_new->image = $request->image;
            $post_new->title_en = $request->title_en;
            $post_new->tags = $request->tags ? json_encode(explode(',',$request->tags)) : json_encode([]);


            $post_new->save();

            $post_new->categories()->sync($request->category_ids);

            if ($request->images) {
                $images = explode(',', $request->images);

                foreach ($images as $image) {
                    $post_image = new PostImage();
                    $post_image->post_id = $post_new->id;
                    $post_image->image = $image;
                    $post_image->save();
                }
            }

            Attachment::where('post_id',$post_new->id)->delete();

            foreach ($request->link_title as $key => $item) {
                $links = explode('http://', $request->link_download[$key]);
                $post_attachment = new Attachment();
                $post_attachment->post_id = $post_new->id;
                $post_attachment->title = $item;
                $post_attachment->links = json_encode($links);
                $post_attachment->save();
            }

            return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
        }
        catch (\Exception $exception){
            return redirect()->back()->with('error', $exception->getMessage());
        }

    }

    public function delete(Request $request)
    {
        $post = Post::findOrFail($request->get('id'));
        $post->categories()->detach();
        $post->delete();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
    }

    public function image_delete(Request $request)
    {
        $postImage = PostImage::findOrFail($request->get('id'));
        $postImage->delete();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
    }

    public function index_image_delete(Request $request)
    {
        $postImage = Post::findOrFail($request->get('id'));
        $postImage->image = '#';
        $postImage->save();

        return redirect()->back()->with('success', 'عملیات با موفقیت انجام شد');
    }

    /*public function upload_image(Request $request)
    {
        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;

            $request->file('upload')->move(public_path('images'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/' . $fileName);
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }*/
}
