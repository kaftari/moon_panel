<?php

namespace App\Http\Controllers\Panel;

use App\Helpers\Date;
use App\Helpers\ImageFactory;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{

    public function json()
    {
        $categories = Category::all()->sortByDesc('id');

        return Datatables::of($categories)
            ->addIndexColumn()
            ->addColumn('action', function ($category) {
                $btn = '<a href="' . route('panel.admin.categoryEdit', ['id' => $category->id]) . '" class="edit btn btn-primary btn-sm">ویرایش</a>' .
                    '<a href="' . route('panel.admin.categoryDelete', ['id' => $category->id]) . '"
                        onclick="return confirm(\'از انجام این عملیات مطمئن هستید؟\')" class="edit btn btn-danger btn-sm">حذف</a>';

                return $btn;
            })
            ->editColumn('post_count', function ($category) {
                return count($category->posts);
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        return view('panel.posts.category_create',[
//            'categories' => Category::all()->sortByDesc('id'),
            'category_types' => Category::category_type(),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'parent_id' => 'required'
        ]);

        try {

            $imageName = null;
            if ($request->file())
                $imageName = ImageFactory::upload($request->file('image'),public_path('uploads/categories/images'));



            $category = new Category();
            $category->name = $request->name;
            $category->description = $request->description;
            $category->slug = $request->slug ? $request->slug :  Str::slug($request->name,'-');
            $category->parent_id = $request->parent_id;
            $category->image = $imageName;
//            $category->type = $request->type;
            $category->save();


            return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
        }
        catch (\Exception $exception)
        {
            return redirect()->back()->with('error',$exception->getMessage());
        }

    }

    public function edit($id)
    {
        return view('panel.posts.category_edit',[
            'category' => Category::findOrFail($id),
//            'categories' => Category::all()->sortByDesc('id'),
            'category_types' => Category::category_type(),
        ]);
    }

    public function patch(Request $request,$id)
    {
        $request->validate([
            'name' => 'required',
            'parent_id' => 'required'
        ]);

        try
        {
            $imageName = null;
            if ($request->file())
                $imageName = ImageFactory::upload($request->file('image'),public_path('uploads/categories/images'));

            $category = Category::findOrFail($id);
            $category->name = $request->name;
            $category->description = $request->description;
            $category->slug = $request->slug;
            $category->parent_id = $request->parent_id;
            $category->image = $request->file() ? $imageName : $category->image;
//            $category->type = $request->type;
            $category->save();

            return redirect()->back()->with('success','عملیات با موفقیت انجام شد');
        }
        catch (\Exception $exception)
        {
            return  redirect()->back()->with('error',$exception->getMessage());
        }

    }
}
